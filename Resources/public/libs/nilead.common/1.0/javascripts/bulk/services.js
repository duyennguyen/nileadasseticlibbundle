angular.module('nilead.common')
    .factory('BulkFactory', ['$rootScope', function ($rootScope) {
        return function (Resource, callables) {
            /**
             * List of pre defined available actions
             *
             * @param {Object} Resource
             *
             * @return {Object}
             */
            var defaultActions = function (Resource) {
                return {
                    /**
                     * Change resources status to active
                     *
                     * @param {Array} items List of items id
                     */
                    enable: function (items) {
                        Resource.enable(items);
                    },

                    /**
                     * Change resources status to active
                     *
                     * @param {Array} items List of items id
                     */
                    disable: function (items) {
                        Resource.disable(items);
                    },

                    /**
                     * Delete resources by the given list of id
                     *
                     * @param {Array} items
                     */
                    delete: function (items) {
                        Resource.delete({ids: items});
                    }
                }
            }(Resource);

            if ('function' === typeof callables) {
                callables = callables(Resource);
            }

            if ('object' !== typeof callables) {
                callables = {};
            }

            if ('function' === typeof callables.actions) {
                callables.actions = callables.actions(Resource);
            }

            if ('object' !== typeof callables.actions) {
                callables.actions = {};
            }

            for (var action in callables.actions) {
                if (null === callables.actions[action]) {
                    callables.actions[action] = defaultActions[action];
                }
            }

            /**
             * Main factory, used for create bulk service
             *
             * @param {Object} Resource
             */
            var BulkFactory = function (Resource) {
                /**
                 * Collection of items registered to this service
                 *
                 * @type {Array}
                 */
                var items = [];

                /**
                 * Collection of selected items
                 *
                 * @type {Array}
                 */
                var selectedItems = [];

                /**
                 * Collection of bulk service registered by name
                 *
                 * @type {Array}
                 */
                var instances = [];

                /**
                 * Cached list of registered items and selected items
                 * Used for one service called more than one times
                 *
                 * @type {Object}
                 */
                var cached = {
                    items: [],
                    selectedItems: []
                };

                return angular.extend({
                    /**
                     * Create new service instance by the given name
                     * If the service name is already registered, just return it self
                     *
                     * @param {String} name
                     *
                     * @return {Object}
                     */
                    createInstance: function (name) {
                        if (undefined !== name) {
                            name = Resource.getRouter().getResource() + '.' + name;

                            if (undefined === instances[name]) {
                                instances[name] = new BulkFactory(Resource);
                            } else {
                                instances[name].reset();
                            }

                            return instances[name];
                        } else {
                            return new BulkFactory(Resource);
                        }
                    },

                    /**
                     * Once service called more than one time,
                     *
                     * From the second one, it should be reset the regiseterd item
                     * and the selected items before return the service
                     */
                    reset: function () {
                        angular.copy(items, cached.items);
                        angular.copy(selectedItems, cached.selectedItems);

                        items = [];
                        selectedItems = [];
                    },

                    /**
                     * Check tis this bulker selected all items
                     *
                     * @return {Boolean}
                     */
                    isSelectedAll: function () {
                        return selectedItems.length === items.length;
                    },

                    /**
                     * Check is this bulker has a partial selected items
                     *
                     * @return {Boolean}
                     */
                    isSelectedPartial: function () {
                        return selectedItems.length && selectedItems.length !== items.length;
                    },

                    /**
                     * Count the number of registered items
                     *
                     * @return {Number}
                     */
                    countItems: function () {
                        return items.length;
                    },

                    /**
                     * Count the number of selected items
                     *
                     * @return {Number}
                     */
                    countSelectedItems: function () {
                        return selectedItems.length;
                    },

                    /**
                     * Sets all registered item to selected
                     */
                    selectAll: function () {
                        angular.copy(items, selectedItems);
                    },

                    /**
                     * Sets no selected item
                     */
                    selectNone: function () {
                        selectedItems = [];
                    },

                    /**
                     * Invert the selected.
                     *
                     * If the item is selected, it'll change to not selected and vice serva
                     */
                    invertSelection: function () {
                        selectedItems = _.difference(items, selectedItems)
                    },

                    /**
                     * Same as invert selection, but only effected on the given item
                     *
                     * @param {Number} id
                     */
                    toggleItem: function (id) {
                        id = parseInt(id);
                        var index = _.indexOf(selectedItems, id);

                        if (-1 === index) {
                            selectedItems.push(id);
                        } else {
                            selectedItems.splice(index, 1);
                        }
                    },

                    /**
                     * Check whether the given item is selected or not
                     *
                     * @param {Number} id
                     *
                     * @return {Boolean}
                     */
                    isActive: function (id) {
                        id = parseInt(id);

                        return -1 !== _.indexOf(selectedItems, id);
                    },

                    /**
                     * Add a single item to registered items list
                     * It also add to selected items list if it stored in
                     * selected items cache list
                     *
                     * @param {Number} id
                     */
                    addItem: function (id) {
                        id = parseInt(id);

                        if (-1 === _.indexOf(items, id)) {
                            items.push(id);

                            if (-1 !== _.indexOf(cached.selectedItems, id)) {
                                selectedItems.push(id);
                            }
                        }
                    },

                    /**
                     * Process the given action, for the selected items
                     *
                     * @param {String} action
                     */
                    process: function (action) {
                        this.actions[action](selectedItems);
                    }
                }, callables);
            };

            return new BulkFactory(Resource);
        };
    }])
;
