angular.module('nilead.cart.cart_item', ['nilead.common', 'nilead.cart.cart', 'nilead.cart.cart_in']).config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('{[').endSymbol(']}')
    }).service('CartItemRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead', bundle: 'cart', resource: 'cart_item', params: {
                _format: 'partial'
            }
        });
    }]).service('CartItemResponse', ['ResponseFactory', 'CartItemRouter', function (ResponseFactory, CartItemRouter) {
        return new ResponseFactory(CartItemRouter);
    }]).service('CartItem', ['ResourceFactory', 'CartItemRouter', 'CartItemResponse', function (ResourceFactory, CartItemRouter, CartItemResponse) {
        return new ResourceFactory(CartItemRouter, CartItemResponse, function (Resource, CartItemRouter, CartItemResponse) {
            return {
                add: function (getParams, postParams, success, error) {
                    var event = this.getEvent('add_item');
                    return Resource.post(CartItemRouter.generate('add', getParams), postParams, function (response) {
                        return CartItemResponse.success(response, success, event);
                    }, function (response) {
                        return CartItemResponse.error(response, error, event);
                    });
                }
            }
        });
    }]).directive('nlCart', ['$compile', 'Cart', 'CartIn', 'CartItem', function ($compile, Cart, CartIn, CartItem) {
        return {
            restrict: 'A', link: function (scope, element, attrs) {

                var cartService = '' == attrs.nlCart ? Cart : CartIn;

                var target = 'undefined' != typeof attrs.nlTarget ? angular.element(attrs.nlTarget) : element;

                var defaultGetParams = {}, defaultPostParams = {};

                if ('undefined' != typeof attrs.nlGetParams) {
                    defaultGetParams = scope.$eval(attrs.nlGetParams);
                }

                var callback = function (object, method, getParams, postParams) {
                    getParams = angular.extend({}, defaultGetParams, getParams || {});
                    postParams = angular.extend({}, defaultPostParams, postParams || {});

                    object[method](getParams, postParams, function (response) {
                        target.html(response.data.template);
                        $compile(target.contents())(scope);

                        return false;
                    });
                };

                scope.removeCartItem = function (getParams, postParams) {
                    callback(CartItem, 'delete', getParams, postParams);

                    return false;
                };

                scope.addCartItem = function (getParams, postParams) {
                    callback(CartItem, 'add', getParams, postParams);

                    return false;
                };

                scope.saveCart = function (getParams, postParams) {
                    callback(cartService, 'saveCart', getParams, postParams);

                    return false;
                };

                scope.clearCart = function (getParams, postParams) {
                    callback(cartService, 'clearCart', getParams, postParams);

                    return false;
                };

                scope.addDiscount = function (getParams, postParams) {
                    callback(cartService, 'addDiscount', getParams, postParams);

                    return false;
                };

                scope.removeDiscount = function (getParams, postParams) {
                    callback(cartService, 'removeDiscount', getParams, postParams);

                    return false;
                };
            }
        }
    }]);
