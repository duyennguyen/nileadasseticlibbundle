angular.module('nilead.address.address', [])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('AddressRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'address',
            resource: 'address',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('AddressResponse', ['ResponseFactory', 'AddressRouter', function (ResponseFactory, AddressRouter) {
        return new ResponseFactory(AddressRouter);
    }])
    .service('Address', ['ResourceFactory', 'AddressRouter', 'AddressResponse', function (ResourceFactory, AddressRouter, AddressResponse) {
        return new ResourceFactory(AddressRouter, AddressResponse);
    }])
    .directive('nlAddressForm', ['GeoBoundary', '$timeout', '$parse', function (GeoBoundary, $timeout, $parse) {
        return {
            scope: {
                model: '=',
                extra: '=',
                options: '=',
                id: '@',
                nlDisabled: '='
            },
            template: function (element, attrs) {
                return angular.element('#address_form\\.html').html();
            },
            link: function (scope, elem, attrs) {
                var geosOrder = ['worldCode', 'countryCode', 'provinceCode', 'districtCode', 'wardCode'];

                scope.extra = {
                    countryCode: [],
                    provinceCode: [],
                    districtCode: [],
                    wardCode: []
                };

                function updateAddress() {
                    scope.address = {
                        worldCode: 'te',
                        countryCode: scope.model.countryCode,
                        provinceCode: scope.model.provinceCode,
                        districtCode: scope.model.districtCode,
                        wardCode: scope.model.wardCode,
                        country: scope.model.country,
                        province: scope.model.province,
                        district: scope.model.district,
                        ward: scope.model.ward
                    };
                }

                //updateAddress();
                // make sure all changes in the main model are reflected here
                scope.$watchCollection('model', function () {
                    updateAddress();
                });

                scope.$watchCollection('address', function () {
                    updateAddress();
                });

                $.fn.select2.amd.require(
                    ['select2/data/array', 'select2/data/minimumInputLength', 'select2/utils'],
                    function (ArrayAdapter, MinimumInputLength, Utils) {
                        function CustomData ($element, options) {
                            ArrayAdapter.__super__.constructor.call(this, $element, options);
                        }

                        Utils.Extend(CustomData, ArrayAdapter);

                        CustomData.prototype.current = function (callback) {
                            var type = this.options.options.type;

                            var current = {
                                id: scope.address[type],
                                text: scope.address[type.replace('Code', '')]
                            };

                            if (current.id == null || current.text == null) {
                                current = {
                                    id: '',
                                    text: ''
                                }
                            }

                            callback([current]);
                        };

                        CustomData.prototype.query = _.debounce(function (params, callback) {

                            var type = this.options.options.type;

                            // find the order of the geo type
                            var index = $.inArray(type, geosOrder);

                            var data = {results: []};


                            var fuzzySearch = function (term, data) {
                                var options = {
                                    caseSensitive: false,
                                    includeScore: false,
                                    shouldSort: true,
                                    threshold: 0.6,
                                    location: 0,
                                    distance: 100,
                                    maxPatternLength: 32,
                                    keys: ["uniqueCode","name"]
                                };
                                var fuse = new Fuse(data, options);
                                return fuse.search(term);
                            };

                            if (index > 0) {
                                var parentId = scope.address[geosOrder[index - 1]];

                                GeoBoundary.getGeoBoundaryMembers(parentId, function (response) {

                                    var matches = fuzzySearch(params.term, response.data);

                                    angular.forEach(matches, function (geo) {
                                        scope.extra[type].push({id: geo.uniqueCode, text: geo.name});
                                        data.results.push({id: geo.uniqueCode, text: geo.name});
                                    });

                                    if (0 == data.results.length) {
                                        scope.extra[type].push({id: params.term, text: params.term});
                                        data.results.push({id: params.term, text: params.term});
                                    }

                                    callback(data);

                                    return response;
                                });
                            }

                        }, 350);

                        CustomData = Utils.Decorate(CustomData, MinimumInputLength);

                        var options = scope.options;

                        if ('object' != typeof options) {
                            options = {};
                        }

                        $timeout(function () {
                            angular.forEach(elem.find('select'), function(e) {
                                var elem = angular.element(e);
                                var model = elem.data('ng-model');
                                var type = model.split('.').pop();

                                elem.select2(
                                    angular.extend({}, options, {
                                        type: type,
                                        minimumInputLength: 2,
                                        dataAdapter: CustomData
                                    })
                                );

                                elem.on('select2:select', function(e) {
                                    var geoId = e.params.data.id;

                                    if ('string' == typeof geoId) {

                                        // Update model
                                        scope.model[type] = geoId;
                                        scope.model[type.replace('Code', '')] = e.params.data.text;

                                        // clear selected value from others
                                        var index = $.inArray(type, geosOrder);

                                        if (index >= 1 && index <= 4) {
                                            GeoBoundary.getGeoBoundaryMembers(geoId);

                                            angular.forEach(geosOrder, function (geo, key) {
                                                if (key > index) {
                                                    scope.model[geo] = undefined;
                                                    scope.model[geo.replace('Code', '')] = undefined;
                                                }
                                            });
                                        }
                                    }
                                });

                                scope.$watchCollection(model, function (newVal, oldVal) {

                                    if (newVal !== oldVal) {
                                        $timeout(function() {
                                            elem.data('select2').dataAdapter.addOptions(
                                                elem.data('select2').dataAdapter.convertToOptions(
                                                    scope.extra[type]
                                                )
                                            );
                                            elem.val(newVal).trigger('change');
                                        }, 0, false);
                                    }
                                });
                            })
                        });
                    }
                );
            }
        };
    }])
    .directive('nlAddressChecker', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            controller: function ($scope) {
                var items = $scope.items = [];

                $scope.check = function (item) {
                    angular.forEach(items, function(item) {
                        item.checked = null;
                    });
                    item.checked = 1;
                };

                $scope.addItem = function (item) {
                    if (items.length == 0) {
                        $scope.select(item);
                    }
                    items.push(item);
                };

                $scope.removeItem = function (item) {
                    var idx = items.indexOf (item);
                    if (idx != -1) {
                        items.splice(idx, 1);
                    }
                };
            }
        };
    }])
;
