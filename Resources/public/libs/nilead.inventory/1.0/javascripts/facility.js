angular.module('nilead.inventory.facility', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('FacilityRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'inventory',
            resource: 'facility',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('FacilityResponse', ['ResponseFactory', 'FacilityRouter', function (ResponseFactory, FacilityRouter) {
        return new ResponseFactory(FacilityRouter);
    }])
    .service('Facility', ['ResourceFactory', 'FacilityRouter', 'FacilityResponse', function (ResourceFactory, FacilityRouter, FacilityResponse) {
        return new ResourceFactory(FacilityRouter, FacilityResponse);
    }])
    .service('FacilityBulk', ['Facility', 'BulkFactory', function (Facility, BulkFactory) {
        return new BulkFactory(Facility, {
            actions: {
                enable: null,
                disable: null,
                delete: null
            }
        });
    }])
;
