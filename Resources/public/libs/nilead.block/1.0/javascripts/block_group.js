angular.module('nilead.block.block_group', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('BlockGroupRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'block',
            resource: 'block_group',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('BlockGroupResponse', ['ResponseFactory', 'BlockGroupRouter', function (ResponseFactory, BlockGroupRouter) {
        return new ResponseFactory(BlockGroupRouter);
    }])

    .service('BlockGroup', ['ResourceFactory', 'BlockRouter', 'BlockResponse', function (ResourceFactory, BlockGroupRouter, BlockGroupResponse) {
        return new ResourceFactory(BlockGroupRouter, BlockGroupResponse);
    }])

    .service('BlockGroupBulk', ['BlockGroup', 'BulkFactory', function (BlockGroup, BulkFactory) {
        return new BulkFactory(Block, {
            actions: {
                enable: null,
                disable: null,
                delete: null
            }
        });
    }]);
