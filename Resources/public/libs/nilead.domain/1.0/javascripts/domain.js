angular.module('nilead.domain.domain', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('DomainRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'domain',
            resource: 'domain',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('DomainResponse', ['ResponseFactory', 'DomainRouter', function (ResponseFactory, DomainRouter) {
        return new ResponseFactory(DomainRouter);
    }])

    .service('Domain', ['ResourceFactory', 'DomainRouter', 'DomainResponse', function (ResourceFactory, DomainRouter, DomainResponse) {
        return new ResourceFactory(DomainRouter, DomainResponse);
    }])
    .service('DomainBulk', ['Domain', 'BulkFactory', function (Domain, BulkFactory) {
        return new BulkFactory(Domain, {
            actions: {
                enable: null,
                disable: null,
                delete: null
            }
        });
    }])
;

