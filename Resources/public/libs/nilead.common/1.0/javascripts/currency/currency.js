var Currency = function () {
    var defaultCurrency;
    var currencies = {};

    return {
        setDefaultCurrency: function (code) {
            defaultCurrency = code;
        },

        getDefaultCurrency: function () {
            return defaultCurrency;
        },

        setCurrencies: function (data) {
            currencies = data;
        },

        getCurrencies: function () {
            return currencies;
        },

        get: function (code) {
            return currencies.hasOwnProperty(code) ? currencies[code] : currencies[this.getDefaultCurrency()];
        },

        fromUnit: function (value, currency) {
            return value / currency.divisor;
        },

        format: function (value, currency) {
            accounting.formatMoney(this.fromUnit(value, currency), currency.symbol, currency.precision, ".", ",");
        }
    }
}();
