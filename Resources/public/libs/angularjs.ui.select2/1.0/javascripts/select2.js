/**
 * Enhanced Select2 Dropmenus
 *
 * @AJAX Mode - When in this mode, your value will be an object (or array of objects) of the data used by Select2
 *     This change is so that you do not have to do an additional query yourself on top of Select2's own query
 * @params [options] {object} The configuration options passed to $.fn.select2(). Refer to the documentation
 */
angular.module('ui.select2', []).value('uiSelect2Config', {})
    .directive('uiSelect2', ['uiSelect2Config', '$timeout', '$parse', function (uiSelect2Config, $timeout, $parse) {
        var options = {};
        if (uiSelect2Config) {
            angular.extend(options, uiSelect2Config);
        }
        return {
            require: 'ngModel',
            priority: 1,
            link: function (scope, elm, attrs, Ctrl) {
                var opts = angular.extend({}, options, $parse(attrs.uiSelect2)(scope));
                var selected = [];

                Ctrl.$parsers.push(function(modelValue) {

                    if ('object' == typeof modelValue) {
                        return _.sortBy(modelValue, function(num){
                            var index = _.lastIndexOf(selected, num);
                            return index > -1 ? index : 0;
                        })
                    }

                    return modelValue;
                });

                $.fn.select2.amd.require(['select2/data/select', 'select2/utils'],
                    function (SelectAdapter, Utils) {
                        function CustomizedSelect($element, options) {
                            CustomizedSelect.__super__.constructor.call(this, $element, options);
                        }

                        Utils.Extend(CustomizedSelect, SelectAdapter);

                        elm.on("select2:selecting", function (e) {
                            selected.push(e.params.args.data.id);
                        });


                        elm.on("select2:unselecting", function (e) {
                            selected = _.without(selected, e.params.args.data.id);
                        });

                        CustomizedSelect.prototype.current = function (callback) {
                            var data = [];
                            var self = this;

                            this.$element.find(':selected').each(function () {
                                var $option = $(this);

                                var option = self.item($option);

                                data.push(option);
                            });

                            data = _.sortBy(data, function(num){
                                var index = _.lastIndexOf(selected, num.id);

                                return index > -1 ? index : 0;
                            });

                            callback(data);
                        };

                        if (opts.hasOwnProperty('dataAdapter')) {
                            Utils.Extend(CustomizedSelect, opts.dataAdapter);
                        }

                        opts.dataAdapter = CustomizedSelect;

                        // Initialize the plugin late so that the injected DOM does not disrupt the template compiler
                        $timeout(function () {
                            elm.select2(opts);
                        });
                });
            }
        };

    }]);


//angular.module('ui.select2', []).value('uiSelect2Config', {})
//    .directive('uiSelect2', ['uiSelect2Config', '$timeout', '$parse', function (uiSelect2Config, $timeout, $parse) {
//        var options = {};
//        if (uiSelect2Config) {
//            angular.extend(options, uiSelect2Config);
//        }
//        return {
//            require: 'ngModel',
//            priority: 1,
//            link: function (scope, elm, attrs) {
//                var opts = angular.extend({}, options, $parse(attrs.uiSelect2)(scope));
//
//                // instance-specific options
//                var getter = $parse(attrs.ngModel);
//
//                scope.$watchCollection(attrs.ngModel, function () {
//                    //elm.val(getter(scope)).trigger('change');
//                });
//
//                //
//                //var getter = $parse(attrs.ngModel);
//                //var setter = getter.assign;
//
//                //elm.on("select2:select", function (e) {
//                //    //var path = attrs.ngModel.split('.');
//                //    //var select = getter(scope);
//                //    //select.push(e.params.data.id);
//                //    setter(scope, e.params.data.id);
//                //    //_.setPath(scope, select, path);
//                //
//                //    //setter(scope, select);
//                //});
//
//                // Initialize the plugin late so that the injected DOM does not disrupt the template compiler
//                $timeout(function () {
//                    elm.select2(opts);
//                });
//            }
//        };
//
//    }]);
