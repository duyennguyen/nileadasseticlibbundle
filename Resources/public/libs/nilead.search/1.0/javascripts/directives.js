angular.module('nilead.module.search')
    .directive('nlSearch', ['NILEAD_EVENTS', 'EventListener', '$compile', '$stateParams', function (NILEAD_EVENTS, EventListener, $compile, $stateParams) {
        return {
            restrict: 'A',
            scope: {
                nlSearcher: '=',
                nlCallback: '&'
            },
            link: function (scope, element, attrs) {
                // We have to do this because the first message might be broadcasted
                // before the directive is initialized
                if ('' != scope.nlSearcher.$filterView) {
                    element.html(scope.nlSearcher.$filterView);
                    $compile(element.contents())(scope);
                }

                // Allow overriding search
                if (! angular.isDefined(attrs.nlCallback)) {
                    scope.search = function (page, reload) {
                        scope.nlSearcher.search({page: page}, reload);
                    }
                } else {
                    scope.search = scope.nlCallback;
                }

                EventListener.once(NILEAD_EVENTS.SEARCH_UPDATE_FILTER_TEMPLATE + scope.nlSearcher.getId(), function () {
                    element.html(scope.nlSearcher.$filterView);
                    $compile(element.contents())(scope);
                });

                EventListener.once(NILEAD_EVENTS.SEARCH_UPDATE_DATA + scope.nlSearcher.getId(), function () {
                    scope.search();
                });

                scope.reset = function () {
                    scope.nlSearcher.reset();
                    scope.search(1);
                }
            }
        };
    }]).directive('nlSearchSorter', ['NILEAD_EVENTS', 'EventListener', function (NILEAD_EVENTS, EventListener) {
        return {
            restrict: 'A',
            scope: {
                nlSearchSorter: '@',
                nlSearcher: '=',
                nlSearchSorterValue: '='
            },
            link: function (scope, element, attrs) {
                element.addClass(scope.nlSearchSorterValue['order'].toLowerCase());

                element.on('click', function () {
                    // toggle order
                    element.removeClass(scope.nlSearchSorterValue['order'].toLowerCase());
                    scope.nlSearchSorterValue['order'] = scope.nlSearchSorterValue['order'] == 'DESC' ? 'ASC' : 'DESC';
                    element.addClass(scope.nlSearchSorterValue['order'].toLowerCase());

                    // update to the searcher data
                    scope.nlSearcher.$data.sorters[scope.nlSearchSorter] = scope.nlSearchSorterValue;

                    // trigger data update
                    EventListener.emit(NILEAD_EVENTS.SEARCH_UPDATE_DATA + scope.nlSearcher.getId());
                });
            }
        }
    }]).directive('nlSearchInput', ['NILEAD_EVENTS', 'EventListener', function (NILEAD_EVENTS, EventListener) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {

                // We have to do this because the first message might be broadcasted
                // before the directive is initialized
                if ('' != scope.nlSearcher.$currentAggs) {
                    scope.nlSearcher.toggleStatus(element, attrs.aggKey, attrs.value, attrs.type);
                }

                // disable inputs upon search
                EventListener.on(NILEAD_EVENTS.SEARCH_START + scope.nlSearcher.getId(), function () {
                    element.prop('disabled', true);
                });

                EventListener.on(NILEAD_EVENTS.SEARCH_UPDATE_FILTER_CURRENT + scope.nlSearcher.getId(), function () {
                    scope.nlSearcher.toggleStatus(element, attrs.aggKey, attrs.value, attrs.type);
                });
            }
        }
    }]).directive('nlSearchSelect2', ['$compile', function ($compile) {
        return {
            restrict: 'A',
            scope: {
                nlSearcher: '=',
                nlSearchParams: '=',
                nlMapping: '='
            },
            compile: function (tElem, tAttrs) {

                return function (scope, iElem, iAttrs) {
                    var searchParams = 'object' == typeof scope.nlSearchParams ? scope.nlSearchParams : {};

                    var mapping = 'object' == typeof scope.nlMapping ? scope.nlMapping : {
                        default: {
                            id: 'id',
                            text: 'name'
                        }
                    };

                    var getMap = function (item, key, type) {
                        if (angular.isUndefined(type)) {
                            type = 'default';
                        }

                        var path = mapping.hasOwnProperty(type) ? mapping[type][key] : mapping.default[key];

                        return _.getPath(item, path);
                    };

                    $.fn.select2.amd.require(['select2/data/array', 'select2/data/minimumInputLength', 'select2/utils'], function (ArrayAdapter, MinimumInputLength, Utils) {
                            function CustomData($element, options) {
                                ArrayAdapter.__super__.constructor.call(this, $element, options);
                            }

                            Utils.Extend(CustomData, ArrayAdapter);

                            CustomData.prototype.query = _.debounce(function (params, callback) {

                                scope.nlSearcher.$data = _.setPath(scope.nlSearcher.$data, params.term, iAttrs.nlSearchSelect2.split('.'), {});

                                scope.nlSearcher.search(angular.extend({_format: 'json'}, searchParams), false, function (response) {
                                    var data = [];

                                    // Group results in groups if necessary
                                    if (_.hasPath(response, 'data.aggs.targets')) {
                                        if (response.data.aggs.targets.length > 1) {
                                            angular.forEach(response.data.aggs.targets, function (target) {

                                                var children = [];
                                                angular.forEach(response.data[target], function (item) {
                                                    children.push({
                                                        id: getMap(item, 'id', target),
                                                        text: getMap(item, 'text', target)
                                                    });
                                                });

                                                if (children.length > 0) {
                                                    data.push({text: target, children: children});
                                                }
                                            });
                                        } else {
                                            angular.forEach(response.data[response.data.aggs.targets[0]], function (item) {
                                                data.push({
                                                    id: getMap(item, 'id', 'default'),
                                                    text: getMap(item, 'text', 'default')
                                                });
                                            });
                                        }
                                    }

                                    callback({results: data});
                                });
                            }, 350);

                            CustomData = Utils.Decorate(CustomData, MinimumInputLength);

                            scope.selectOptions = function (options) {
                                if ('object' != typeof options) {
                                    options = {};
                                }

                                return angular.extend(options, {
                                    minimumInputLength: 3,
                                    dataAdapter: CustomData
                                });
                            };

                            iElem.html(iAttrs.template);
                            $compile(iElem.contents())(scope);
                        });
                }
            }
        };
    }]).directive('nlSearchSimple', ['$compile', function ($compile) {
        return {
            restrict: 'A',
            scope: {
                nlSearcher: '=',
                nlInit: '=',
                nlSubmitButton: '@',
                nlTarget: '@',
                nlTemplate: '@'
            }, controller: function ($scope) {
                var $this = this;
                // this is for the pagination callback if any
                $scope.$parent.paginationCallback = function (page, $event) {
                    $this.search(page);

                    $event.preventDefault();
                };

                this.search = function (page, reload) {

                    $scope.nlSearcher.search({
                        page: page,
                        _format: 'partial',
                        include_metadata: 1,
                        include_full_agg: 0
                        }, reload, function (response) {
                            var element =  angular.element($scope.nlTarget);
                            element.html(response.data.template);
                            $compile(element.contents())($scope.$parent);

                            return false;
                        });
                };
            }, link: function (scope, element, attrs, Ctrl) {

                element.append($compile(scope.nlTemplate)(scope));

                scope.search = function () {
                    Ctrl.search(1);
                };

                if (scope.nlInit) {
                    Ctrl.search(1);
                }
            }
        };
    }]).directive('nlSearchGlobal', ['$compile', '$document', function ($compile, $document) {
        return {
            restrict: 'A',
            scope: {
                nlSearcher: '=',
                nlTarget: '@'
            },
            link: function (scope, element, attrs) {
                var target = angular.element(scope.nlTarget);

                element.on('keyup', _.debounce(function () {

                    if (element.val().length > 2) {
                        element.prop('disabled', true);
                        scope.nlSearcher.search({
                            _format: 'partial', include_metadata: 0, include_full_agg: 0, limit: 12, q: {
                                queries: {
                                    global: {value: element.val()}
                                }
                            }
                        }).then(function (response) {
                            target.html(response.data.template).show().addClass('open');
                            $compile(target.contents())(scope);

                            element.prop('disabled', false);

                            $document.on('click.global_search', function (e) {

                                var isClickedElementChildOfPopup = target.find(e.target).length > 0;

                                if (isClickedElementChildOfPopup) {
                                    return;
                                }

                                target.removeClass('open').hide();

                                $document.off('click.global_search');

                            });
                        });
                    }
                }, 350));
            }
        };
    }]);

