angular.module('nilead.product.property_group', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('PropertyGroupRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'product',
            resource: 'property_group',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('PropertyGroupResponse', ['ResponseFactory', 'PropertyGroupRouter', function (ResponseFactory, PropertyGroupRouter) {
        return new ResponseFactory(PropertyGroupRouter);
    }])
    .service('PropertyGroup', ['ResourceFactory', 'PropertyGroupRouter', 'PropertyGroupResponse', function (ResourceFactory, PropertyGroupRouter, PropertyGroupResponse) {
        return new ResourceFactory(PropertyGroupRouter, PropertyGroupResponse);
    }])
    .service('PropertyGroupBulk', ['PropertyGroup', 'BulkFactory', function (PropertyGroup, BulkFactory) {
        return new BulkFactory(PropertyGroup, {
            actions: {
                delete: null
            }
        });
    }])
;
