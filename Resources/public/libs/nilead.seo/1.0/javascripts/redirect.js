angular.module('nilead.seo.redirect', ['nilead.common', 'blockUI'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })

    .service('RedirectRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'seo',
            resource: 'redirect',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('RedirectResponse', ['ResponseFactory', 'RedirectRouter', function (ResponseFactory, RedirectRouter) {
        return new ResponseFactory(RedirectRouter);
    }])
    .service('Redirect', ['ResourceFactory', 'RedirectRouter', 'RedirectResponse', 'Resource', function (ResourceFactory, RedirectRouter, RedirectResponse, Resource) {
        return new ResourceFactory(RedirectRouter, RedirectResponse);
    }])
    .service('RedirectBulk', ['Redirect', 'BulkFactory', function (Redirect, BulkFactory) {
        return new BulkFactory(Redirect, {
            actions: {
                delete: null
            }
        });
    }])