angular.module('nilead.common')
    .provider('Resource', function () {
        this.$get = ['ResponseProcessor', 'Restangular', function (ResponseProcessor, Restangular) {

            var helper = function () {
                return {
                    success: function (response, callback) {
                        if ('function' === typeof callback) {
                            return callback(response);
                        }

                        return response;
                    },

                    error: function (response, callback) {
                        if ('function' === typeof callback) {
                            return callback(response);
                        }

                        return response;
                    }
                }
            }();

            return {
                get: function (url, success, error, config) {
                    return Restangular
                        .withConfig(function (RestangularConfigurer) {
                            if (undefined !== config) {
                                config(RestangularConfigurer);
                            }
                        })
                        .oneUrl('url', url)
                        .get()
                        .then(function (response) {

                            return helper.success(response, success);
                        }, function (response) {

                            return helper.error(response, error);
                        })
                        ;
                },

                post: function (url, data, success, error) {
                    if (data instanceof FormData) {
                        return Restangular
                            .withConfig(function (RestangularConfigurer) {
                                RestangularConfigurer.setDefaultHeaders({'Content-Type': undefined});
                            })
                            .oneUrl('url', url)
                            .withHttpConfig({transformRequest: angular.identity})
                            .customPOST(data)
                            .then(function (response) {
                                return helper.success(response, success);
                            }, function (response) {
                                return helper.error(response, error);
                            })
                            ;
                    } else {
                        return Restangular
                            .oneUrl('url', url)
                            .post('', data)
                            .then(function (response) {
                                return helper.success(response, success);
                            }, function (response) {
                                return helper.error(response, error);
                            })
                            ;
                    }
                },

                put: function (url, data, success, error) {
                    return Restangular
                        .oneUrl('url', url)
                        .customPUT(data)
                        .then(function (response) {
                            return helper.success(response, success);
                        }, function (response) {
                            return helper.error(response, error);
                        })
                        ;
                },

                remove: function (url, data, success, error) {
                    return Restangular.oneUrl('url', url).remove('', data)
                        .then(function (response) {
                            return helper.success(response, success);
                        }, function (response) {
                            return helper.error(response, error);
                        })
                        ;
                }
            }
        }]
    })
    .factory('ResourceFactory', ['$rootScope', '$state', 'Resource', 'File', 'NILEAD_EVENTS', 'EventListener', function ($rootScope, $state, Resource, File, NILEAD_EVENTS, EventListener) {
        return function (Router, Response, callables) {
            if ('function' === typeof callables) {
                callables = callables(Resource, Router, Response);
            }

            if ('object' !== typeof callables) {
                callables = {};
            }

            return new function ($rootScope, $state, Router, Response) {
                return angular.extend({
                    getRouter: function () {
                        return Router;
                    },

                    getResponse: function () {
                        return Response;
                    },

                    getEvent: function (action) {
                        return {
                            event: (Router.getResource() + '_' + action).toUpperCase(),
                            resource: Router.getResource(),
                            state: $state.current.name
                        }
                    },

                    index: function (params, success, error) {
                        var event = this.getEvent('index');
                        return Resource.get(Router.generate('index', params),
                            function (response) {
                                return Response.success(response, success, event);
                            },
                            function (response) {
                                return Response.error(response, error, event)
                            }
                        );
                    },

                    show: function (id, success, error) {
                        var event = this.getEvent('show');
                        return Resource.get(Router.generate('show', {id: id}),
                            function (response) {
                                return Response.success(response, success, event)
                            },
                            function (response) {
                                return Response.error(response, error, event)
                            }
                        );
                    },

                    get: function (action, params, success, error) {
                        var event = this.getEvent(action);
                        return Resource.get(Router.generate(action, params),
                            function(response) {
                                return Response.success(response, success, event);
                            },
                            function(response) {
                                return Response.error(response, error, event);
                            }
                        );
                    },

                    post: function (action, params, data, success, error) {
                        var event = this.getEvent(action);
                        return Resource.post(Router.generate(action, params), data,
                            function(response) {
                                return Response.success(response, success, event);
                            },
                            function(response) {
                                return Response.error(response, error, event);
                            }
                        );
                    },

                    create: function (data, success, error) {
                        var event = this.getEvent('create');
                        return Resource.post(Router.generate('create'), data,
                            function(response) {
                                return Response.success(response, success, event);
                            },
                            function(response) {
                                return Response.error(response, error, event);
                            }
                        );
                    },

                    update: function (id, data, success, error) {
                        var event = this.getEvent('update');

                        return Resource.post(Router.generate('update', {id: id}), data,
                            function(response) {
                                return Response.success(response, success, event);
                            },
                            function(response) {
                                return Response.error(response, error, event);
                            }
                        );
                    },

                    delete: function (getParams, postParams, success, error) {
                        getParams = getParams || {};
                        postParams = postParams || {};

                        var event = this.getEvent('delete');
                        return Resource.post(Router.generate('delete', angular.extend({include_metadata: true, paginate: true}, getParams)), postParams,
                            function(response) {
                                return Response.success(response, success, event);
                            },
                            function(response) {
                                return Response.error(response, error, event);
                            }
                        );
                    },

                    transit: function (getParams, postParams, success, error) {
                        getParams = getParams || {};
                        postParams = postParams || {};

                        var event = this.getEvent('transit');

                        return Resource.post(Router.generate('transit', getParams), postParams,
                            function(response) {
                                return Response.success(response, success, event);
                            },
                            function(response) {
                                return Response.error(response, error, event);
                            }
                        );
                    },

                    toggleEnable: function (id, success, error) {
                        var event = this.getEvent('toggle_enable');
                        return Resource.post(Router.generate('toggle_enable', angular.extend({include_metadata: true, paginate: true}, $state.params)), {ids: id},
                            function(response) {
                                return Response.success(response, success, event);
                            },
                            function(response) {
                                return Response.error(response, error, event);
                            }
                        );
                    },

                    enable: function (id, success, error) {
                        var event = this.getEvent('enable');
                        return Resource.post(Router.generate('toggle_enable', angular.extend({include_metadata: true, paginate: true}, $state.params)), {ids: id, enabled: true},
                            function(response) {
                                return Response.success(response, success, event);
                            },
                            function(response) {
                                return Response.error(response, error, event);
                            }
                        );
                    },

                    disable: function (id, success, error) {
                        var event = this.getEvent('disable');
                        return Resource.post(Router.generate('toggle_enable', angular.extend({include_metadata: true, paginate: true}, $state.params)), {ids: id, enabled: false},
                            function(response) {
                                return Response.success(response, success, event);
                            },
                            function(response) {
                                return Response.error(response, error, event);
                            }
                        );
                    }
                }, callables);
            }($rootScope, $state, Router, Response);
        };
    }])
;
