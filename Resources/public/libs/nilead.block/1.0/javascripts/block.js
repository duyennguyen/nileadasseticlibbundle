angular.module('nilead.block.block', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('BlockRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'block',
            resource: 'block',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('BlockResponse', ['ResponseFactory', 'BlockRouter', function (ResponseFactory, BlockRouter) {
        return new ResponseFactory(BlockRouter);
    }])

    .service('Block', ['ResourceFactory', 'BlockRouter', 'BlockResponse', function (ResourceFactory, BlockRouter, BlockResponse) {
        return new ResourceFactory(BlockRouter, BlockResponse);
    }])

    .service('BlockBulk', ['Block', 'BulkFactory', function (Block, BulkFactory) {
        return new BulkFactory(Block, {
            actions: {
                enable: null,
                disable: null,
                delete: null
            }
        });
    }]);
