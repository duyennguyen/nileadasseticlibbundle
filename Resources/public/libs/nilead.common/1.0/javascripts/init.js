var ng = {
    deps: {}, dependsOn: function ($ns, $dep) {

        if (_.isString($dep)) {
            $dep = [$dep];
        }

        if (undefined === this.deps[$ns]) {
            this.deps[$ns] = [];
        }

        this.deps[$ns] = _.union(this.deps[$ns], $dep);
    }, getDependencies: function ($ns) {
        if (undefined === this.deps[$ns]) {
            return [];
        } else {
            return this.deps[$ns]
        }
    }
};

var nl = {
    utility: {
        path: function (obj, path, def) {

            for (var i = 0, path = path.split('.'), len = path.length; i < len; i ++) {
                if (! obj || typeof obj !== 'object') return def;
                obj = obj[path[i]];
            }

            if (obj === undefined) return def;
            return obj;
        }
    },
    eventListener: smokesignals.convert({}),
    events: {
        SEARCH_START: 'NILEAD_SEARCH_START',
        SEARCH_END: 'NILEAD_SEARCH_END',
        UPDATE_VIEW: 'NILEAD_UPDATE_VIEW',
        SEARCH_UPDATE_DATA: 'NILEAD_SEARCH_UPDATE_DATA',
        SEARCH_UPDATE_FILTER_CURRENT: 'NILEAD_SEARCH_UPDATE_FILTER_CURRENT',
        SEARCH_UPDATE_FILTER_TEMPLATE: 'NILEAD_SEARCH_UPDATE_FILTER_TEMPLATE',
        SEARCH_ERROR: 'NILEAD_SEARCH_ERROR',
        SEARCH_SUCCESS: 'NILEAD_SEARCH_SUCCESS',
        RESOURCE_RESPONSE_SUCCESS: 'NILEAD_RESOURCE_RESPONSE_%event%_SUCCESS',
        RESOURCE_RESPONSE_ERROR: 'NILEAD_RESOURCE_RESPONSE_%event%_ERROR'
    }
};

String.prototype.hashCode = function () {
    var hash = 0, i, chr, len;
    if (this.length == 0) return hash;
    for (i = 0, len = this.length; i < len; i ++) {
        chr = this.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};
