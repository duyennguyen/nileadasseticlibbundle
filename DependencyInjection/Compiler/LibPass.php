<?php
/**
 * Created by Rubikin Team.
 * Date: 10/23/13
 * Time: 12:52 AM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\AsseticLibBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class LibPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $libs = [];
        if ($container->hasParameter('nilead.assetic.libs')) {
            $libs = $container->getParameter('nilead.assetic.libs');
        }

        // lets find all the libraries we have
        $finder = new Finder();
        $finder->directories()->in(__DIR__ . '/../../Resources/public/libs')->depth(0);

        /** @var SplFileInfo $lib */
        foreach ($finder as $lib) {
            if (file_exists($configFile = $lib->getRealPath() . '/config.yml')) {
                $versionFinder = new Finder();
                $versionFinder->directories()->in($lib->getRealPath())->depth(0);

                $libName = 'libs/' . $lib->getBasename();
                $libs[$libName] = [
                    'realPath' => $lib->getRealPath(),
                    'rootPath' => __DIR__ . '/../../Resources/public',
                    'bundle' => 'NileadAsseticLibBundle',
                    'versions' => []
                ];

                $libs[$libName]['config_file'] = $configFile;

                /** @var SplFileInfo $version */
                foreach ($versionFinder as $version) {
                    if (file_exists($configFile = $version->getRealPath() . '/config.yml')) {
                        $libs[$libName]['versions'][$version->getBasename()]['config_file'] = $configFile;
                    } else {
                        $libs[$libName]['versions'][$version->getBasename()]['config_file'] = false;
                    }
                }
            }
        }

        $container->setParameter('nilead.assetic.libs', $libs);
    }
}
