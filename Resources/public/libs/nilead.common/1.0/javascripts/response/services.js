angular.module('nilead.common')
    .provider('ResponseProcessor',  function() {
        this.$get = ['$window', 'Alert', 'RouteGenerator', '$state', 'NILEAD_EVENTS', 'EventListener', 'growl', function($window, Alert, RouteGenerator, $state, NILEAD_EVENTS, EventListener, growl) {
            return {
                $views: {},
                setView: function(key, view) {
                    this.$views[key] = view;
                    // dispatch event
                    EventListener.emit(NILEAD_EVENTS.UPDATE_VIEW + key);
                },
                getView: function(key) {
                    return this.$views[key];
                },
                processMessages: function(Alert, response) {
                    Alert.reset();
                    var messages = [];

                    if (messages = _.getPath(response, 'data.data.messages')) {
                        angular.forEach(messages, function(message) {
                            if (-1 === _.indexOf(['error', 'warning', 'info', 'success'], message.type)) {
                                message.type = 'info';
                            }

                            growl[message.type](message.content, {});
                            //Alert.add(message.type, 'global', message.content);
                        });
                    }

                    if (meta = _.getPath(response, 'data.data.meta')) {
                        document.title = meta.title;
                    }
                },
                processResponse: function(response) {
                    if (typeof response != 'undefined' && response.hasOwnProperty('data')) {
                        if (_.hasPath(response.data, ['data', 'views'])) {
                            var $this = this;

                            angular.forEach(response.data.data.views, function(value, key) {
                                $this.setView(key, value);
                            });
                        }

                        if (response.data.hasOwnProperty('redirect')) { // redirect

                            var redirect = response.data.redirect;
                            if (redirect.params.hasOwnProperty('force') && redirect.params.force) {
                                if($state.current.name == RouteGenerator.generateState(redirect.state)) {
                                    $window.location.reload();
                                } else {
                                    $window.location.href = redirect.url;
                                }
                            }

                            var reload = redirect.params.hasOwnProperty('reload') ? redirect.params.reload : true;

                            $state.go(RouteGenerator.generateState(redirect.state), redirect.params, {
                                reload: reload,
                                location: true
                            });
                        }
                    }
                },
                processSuccess: function(response) {
                    if (typeof response !== "undefined") {
                        this.processMessages(Alert, response);
                    }

                    this.processResponse(response);
                },
                processError: function(response) {
                    if (typeof response !== "undefined") {
                        this.processMessages(Alert, response);
                    }

                    this.processResponse(response);
                },
                getAlert: function() {
                    return Alert;
                }
            }
        }]
    })
    .factory('ResponseFactory', ['$rootScope', '$state', '$stateParams', 'growl', 'ResponseProcessor', 'EventListener', 'NILEAD_EVENTS', function($rootScope, $state, $stateParams, growl , ResponseProcessor, EventListener, NILEAD_EVENTS) {
        return function(Router, callables) {
            var helper = function(Router) {
                return {
                    isHybridFormat: function() {
                        return 'partial' === Router.getFormat();
                    },

                    isApiFormat: function() {
                        return 'json' === Router.getFormat() || 'xml' === Router.getFormat();
                    },

                    params: function(response, extraParams) {
                        var params;

                        if ('object' !== typeof extraParams) {
                            extraParams = {};
                        }

                        switch (Router.getAction()) {
                            case 'show':
                            case 'update':
                                params = {id: response.data.data[Router.getResource()].id}

                                break;
                            default:
                                params = {};
                        }

                        angular.forEach(extraParams, function(element, index) {
                            if ('object' === typeof element && element.hasOwnProperty('value')) {
                                this[index] = element.value;
                            } else {
                                this[index] = element;
                            }
                        }, params);

                        return params;
                    }
                }
            }(Router);

            if ('function' === typeof callables) {
                callables = callables(Router, helper);
            } else if ('object' !== typeof callables) {
                callables = {};
            }

            return new function($rootScope, $state, $stateParams, Router, helper) {
                return angular.extend(callables, {
                    success: function(response, callback, event) {
                        this.response = response;

                        if ('object' === typeof event) {
                            EventListener.emit(NILEAD_EVENTS.RESOURCE_RESPONSE_SUCCESS
                                .replace('%event%', event.event)
                                .toUpperCase(), response)
                            ;
                        }

                        ResponseProcessor.processSuccess(response);

                        // callback function
                        var callbackResponse = '';
                        switch (typeof callback) {
                            case 'function':
                                if (false === (callbackResponse = callback(response))) {
                                    return response;
                                }
                                break;
                            case 'boolean':
                                return response;
                        }

                        if (helper.isHybridFormat()) {
                            //if ('function' === typeof $state.current.next) {
                            //    nextState = $state.current.next(response.data.data[Router.getResource()], $rootScope.$params);
                            //} else if (response.data.template) {
                            //    nextState = {
                            //        name: $state.current.name,
                            //        params: angular.extend({}, $rootScope.$params || {}, $stateParams || {})
                            //    };
                            //
                            //    $state.current.template = response.data.template;
                            //}
                            if (response.data.template) {

                                $state.current.template = response.data.template;
                                //if (typeof event === 'object' && $state.current.name != '' && event.state != $state.current.name) {


                                // if (typeof event == 'object' && $state.current.name != '' && event.state != $state.current.name) {
                                //
                                // @vunguyen: We do not need to check the state name anymore, It's not actual work correctly
                                //            If any request doesn't want to reload template, just pass "false" to callback.
                                $state.transitionTo($state.current.name, helper.params(response, angular.extend({}, $rootScope.$params || {}, $stateParams || {})), {
                                    notify: true,
                                    reload: true
                                });

                                delete $state.current.template;
                                // }
                            }
                        }

                        if(callbackResponse) {
                            return callbackResponse;
                        }

                        return response;
                    },

                    error: function(response, callback, error) {
                        this.response = response;

                        if ('object' === typeof event) {
                            EventListener.emit(NILEAD_EVENTS.RESOURCE_RESPONSE_ERROR
                                .replace('%event%', event.event)
                                .toUpperCase(), response);
                        }

                        ResponseProcessor.processError(response);

                        // callback function
                        var callbackResponse = '';
                        switch (typeof callback) {
                            case 'function':
                                if (false === (callbackResponse = callback(response))) {
                                    return response;
                                }
                                break;
                            case 'boolean':
                                return response;
                        }

                        if (helper.isHybridFormat() && undefined !== $state.current.templateProvider) {
                            if (response.data.template) {
                                $state.current.template = response.data.template;

                                //if (typeof event == 'object' && $state.current.name != '' && event.state != $state.current.name) {
                                    $state.transitionTo($state.current.name, helper.params(response, angular.extend({}, $rootScope.$params || {}, $stateParams || {})), {
                                        notify: true,
                                        reload: true
                                    });

                                    delete $state.current.template;
                                //}
                            }
                        }

                        if(callbackResponse) {
                            return callbackResponse;
                        }

                        return response;
                    }
                });
            }($rootScope, $state, $stateParams, Router, helper);
        }
    }])
;
