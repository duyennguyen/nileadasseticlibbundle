angular.module('nilead.common')
    .directive('nlPaginator', ['$state', '$stateParams', function ($state, $stateParams) {
        return {
            templateUrl: 'paginator.html',
            scope: {
                nlPaginator: '=',
                nlCallback: '='
            },
            link: function (scope, elem, attrs) {
                scope.$watch('nlPaginator', function(nlPaginator) {
                    if(nlPaginator && nlPaginator.totalCount) {
                        scope.nlPaginator.pageCount  = Math.ceil(nlPaginator.totalCount / nlPaginator.numItemsPerPage);
                        scope.nlPaginator.current = parseInt(nlPaginator.current);
                        scope.nlPaginator.firstItem = (nlPaginator.numItemsPerPage * (nlPaginator.current - 1)) + 1;
                        scope.nlPaginator.lastItem = scope.nlPaginator.firstItem + nlPaginator.currentItemCount - 1;

                        if (nlPaginator.current > 1) {
                            scope.nlPaginator.previous = nlPaginator.current - 1;
                        }

                        if (nlPaginator.current < scope.nlPaginator.pageCount) {
                            scope.nlPaginator.next = nlPaginator.current + 1;
                        }

                        if (nlPaginator.pageRange < scope.nlPaginator.pageCount) {
                            var $beforeCurrent = Math.ceil(nlPaginator.pageRange / 2)-1;
                            var $afterCurrent = Math.floor(nlPaginator.pageRange / 2);
                            if (nlPaginator.current - $beforeCurrent <= 1) {
                                scope.nlPaginator.pageRngeOffsetFrom = 1;
                                scope.nlPaginator.pageRngeOffsetTo = nlPaginator.pageRange;
                                scope.nlPaginator.pageRangeNext = (nlPaginator.current + nlPaginator.pageRange) > scope.nlPaginator.pageCount ? scope.nlPaginator.pageCount : (nlPaginator.current + nlPaginator.pageRange);
                            } else if (nlPaginator.current + $afterCurrent >= scope.nlPaginator.pageCount) {
                                scope.nlPaginator.pageRngeOffsetTo = scope.nlPaginator.pageCount;
                                scope.nlPaginator.pageRngeOffsetFrom = scope.nlPaginator.pageCount - nlPaginator.pageRange + 1;
                                scope.nlPaginator.pageRangePrev = (nlPaginator.current - nlPaginator.pageRange) < 1 ? 1 : (nlPaginator.current - nlPaginator.pageRange);
                            } else {
                                scope.nlPaginator.pageRngeOffsetFrom = nlPaginator.current - $beforeCurrent;
                                scope.nlPaginator.pageRngeOffsetTo = nlPaginator.current + $afterCurrent;
                                scope.nlPaginator.pageRangeNext = (nlPaginator.current + nlPaginator.pageRange) > scope.nlPaginator.pageCount ? scope.nlPaginator.pageCount : (nlPaginator.current + nlPaginator.pageRange);
                                scope.nlPaginator.pageRangePrev = (nlPaginator.current - nlPaginator.pageRange) < 1 ? 1 : (nlPaginator.current - nlPaginator.pageRange);
                            }

                            scope.pagesInRange = _.range(scope.nlPaginator.pageRngeOffsetFrom, scope.nlPaginator.pageRngeOffsetTo+1);
                        } else {
                            scope.pagesInRange = _.range(1, scope.nlPaginator.pageCount+1);
                        }
                    }
                });

                scope.link = function (stateParams) {
                    return undefined === stateParams.page ? null : $state.href(attrs.route, angular.extend({}, $stateParams, stateParams));
                };

                scope.$callback = function ($event, page) {
                    if (typeof scope.nlCallback === 'function') {
                        if(!scope.nlCallback(page)) {
                            $event.preventDefault();
                        }
                    }
                }
            }
        };
    }])
;
