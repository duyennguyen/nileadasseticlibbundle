angular.module('nilead.discount.discount', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('DiscountRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'discount',
            resource: 'discount',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('DiscountResponse', ['ResponseFactory', 'DiscountRouter', function (ResponseFactory, DiscountRouter) {
        return new ResponseFactory(DiscountRouter);
    }])
    .service('Discount', ['ResourceFactory', 'DiscountRouter', 'DiscountResponse', function (ResourceFactory, DiscountRouter, DiscountResponse) {
        return new ResourceFactory(DiscountRouter, DiscountResponse);
    }])
    .service('DiscountBulk', ['Discount', 'BulkFactory', function (Discount, BulkFactory) {
        return new BulkFactory(Discount, {
            actions: {
                enable: null,
                disable: null
            }
        });
    }])
;
