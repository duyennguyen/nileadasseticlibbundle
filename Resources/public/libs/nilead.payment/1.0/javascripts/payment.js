angular.module('nilead.payment.payment', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('PaymentRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'payment',
            resource: 'payment',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('PaymentResponse', ['ResponseFactory', 'PaymentRouter', function (ResponseFactory, PaymentRouter) {
        return new ResponseFactory(PaymentRouter);
    }])
    .service('Payment', ['ResourceFactory', 'PaymentRouter', 'PaymentResponse', function (ResourceFactory, PaymentRouter, PaymentResponse) {
        return new ResourceFactory(PaymentRouter, PaymentResponse);
    }])
;
