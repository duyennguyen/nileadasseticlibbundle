angular.module('nilead.product.option', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('OptionRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'product',
            resource: 'option',
            params: {
                _format: 'partial'
            }
        });
    }])

    .service('OptionResponse', ['ResponseFactory', 'OptionRouter', 'ImageAwareResponseFactory', function (ResponseFactory, OptionRouter, ImageAwareResponseFactory) {

        return ImageAwareResponseFactory(new ResponseFactory(OptionRouter), function (response, resource) {
            var path = ['data', 'data', resource, 'values'], images = {};
            if (_.hasPath(response, path)) {
                angular.forEach(_.getPath(response, path), function (optionValue) {
                    if(optionValue.image) {
                        images[optionValue.image.id] = optionValue.image;
                    }
                });
            }

            return images;
        });

    }])

    .service('Option', ['ResourceFactory', 'OptionRouter', 'OptionResponse', function (ResourceFactory, OptionRouter, OptionResponse) {
        return new ResourceFactory(OptionRouter, OptionResponse , function(Resource, OptionRouter, OptionResponse){
            return {
                listOptionValue: function(id, success, error) {
                    var event = this.getEvent('list_option_value');
                    return Resource.get(OptionRouter.generate('list_option_value', {id: id}), {},
                        function (response) {
                            return OptionResponse.success(response, success, event);
                        },
                        function (response) {
                            return OptionResponse.error(response, error, event);
                    });
                }}
        });
    }])
    .service('OptionBulk', ['Option', 'BulkFactory', function (Option, BulkFactory) {
        return new BulkFactory(Option, {
            actions: {
                delete: null
            }
        });
    }])

;
