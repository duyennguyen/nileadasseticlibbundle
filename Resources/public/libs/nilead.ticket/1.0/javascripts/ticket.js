angular.module('nilead.ticket.ticket', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('TicketRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'ticket',
            resource: 'ticket',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('TicketResponse', ['ResponseFactory', 'ImageAwareResponseFactory', 'TicketRouter', function (ResponseFactory, ImageAwareResponseFactory, TicketRouter) {
        return ImageAwareResponseFactory(new ResponseFactory(TicketRouter));
    }])
    .service('Ticket', ['ResourceFactory', 'TicketRouter', 'TicketResponse', function (ResourceFactory, TicketRouter, TicketResponse) {
        return new ResourceFactory(TicketRouter, TicketResponse, function(Resource, TicketRouter, TicketResponse){
            return {
                hold: function(id, data, success, error) {
                    var event = this.getEvent('hold');
                    return Resource.post(TicketRouter.generate('hold', { id: id, _format: 'json' }), null,
                        function (response) {
                            return TicketResponse.success(response, success, event);
                        },
                        function (response) {
                            return TicketResponse.error(response, error, event);
                        });
                },
                feedback: function(data, success, error) {
                    var event = this.getEvent('feedback');
                    return Resource.post(TicketRouter.generate('feedback', { _format: 'json' }), data,
                        function (response) {
                            return TicketResponse.success(response, success, event);
                        },
                        function (response) {
                            return TicketResponse.error(response, error, event);
                        });
                },
                unHold: function(id, data, success, error) {
                    var event = this.getEvent('unHold');
                    return Resource.post(TicketRouter.generate('unHold', { id: id, _format: 'json' }), null,
                        function (response) {
                            return TicketResponse.success(response, success, event);
                        },
                        function (response) {
                            return TicketResponse.error(response, error, event);
                        });
                }
            };
        });

    }])
    .service('TicketBulk', ['Ticket', 'BulkFactory', function (Ticket, BulkFactory) {
        return new BulkFactory(Ticket, {
            actions: {
                enable: null,
                disable: null,
                delete: null
            }
        });
    }])
    .service('TicketSearch', ['TicketRouter', 'SearchFactory', function (TicketRouter, SearchFactory) {
        return function (id, preset, action) {
            if ( ! angular.isDefined(action)) {
                action = 'index';
            }

            return SearchFactory(id, {
                action: action,
                router: TicketRouter,
                entities: [TicketRouter.getResource()]
            }, 'ticket', preset);
        }
    }])
;
