if (!RedactorPlugins) var RedactorPlugins = {};

(function ($) {
    RedactorPlugins.nlimage = function () {
        return {
            init: function () {

                // add some more langs text
                $.extend(this.opts.langs.en, {
                    image_size: 'Image Size',
                    original: 'Original',
                    grande: 'Grande',
                    large: 'Large',
                    medium: 'Medium',
                    small: 'Small',
                    thumb: 'Thumb'
                });

                // redefine image edit modal
                this.opts.modal.imageEdit = String()
                + '<section id="redactor-modal-image-edit">'
                + '<label>' + this.lang.get('title') + '</label>'
                + '<input type="text" id="redactor-image-title" />'
                + '<label class="redactor-image-link-option">' + this.lang.get('link') + '</label>'
                + '<input type="text" id="redactor-image-link" class="redactor-image-link-option" />'
                + '<label class="redactor-image-link-option"><input type="checkbox" id="redactor-image-link-blank"><span></span> ' + this.lang.get('link_new_tab') + '</label>'
                + '<label class="redactor-image-position-option">' + this.lang.get('image_position') + '</label>'
                + '<select class="redactor-image-position-option" id="redactor-image-align">'
                + '<option value="none">' + this.lang.get('none') + '</option>'
                + '<option value="left">' + this.lang.get('left') + '</option>'
                + '<option value="center">' + this.lang.get('center') + '</option>'
                + '<option value="right">' + this.lang.get('right') + '</option>'
                + '</select>'
                + '<div id="redactor-image-size-wrapper">'
                + '<label class="redactor-image-position-option">' + this.lang.get('image_size') + '</label>'
                + '<select class="redactor-image-position-option" id="redactor-image-size">'
                + '<option value="thumb">' + this.lang.get('thumb') + '</option>'
                + '<option value="small">' + this.lang.get('small') + '</option>'
                + '<option value="medium">' + this.lang.get('medium') + '</option>'
                + '<option value="large">' + this.lang.get('large') + '</option>'
                + '<option value="grande">' + this.lang.get('grande') + '</option>'
                + '<option value="original">' + this.lang.get('original') + '</option>'
                + '</div>';
                +'</section>';

                var $this = this;
                $.extend(this.image, {
                    load: function () {
                        var template = $($this.opts.nlImageTemplate);

                        //get your angular element
                        var elem = angular.element(document.querySelector('body'));

                        //get the injector.
                        var injector = elem.injector();

                        var ImageSearch = injector.get('ImageSearch');

                        //get the service.
                        var $compile = injector.get('$compile');

                        if (template.length) {
                            $this.modal.addTemplate('nlimage', template.html());
                            $this.modal.load('nlimage', $this.lang.get('image'), 700);

                            var $modal = $this.modal.getModal();

                            var $box = $('<div id="redactor-image-manager-box" style="overflow: auto; height: 300px;" class="redactor-tab redactor-tab2">').hide();
                            $modal.append($box);

                            $this.selection.save();
                            $this.modal.show();

                            $this.$element.scope().redactorImageSearcher = ImageSearch('redactor_image_listing');

                            $this.$element.scope().redactorImageSearcher.$params.template = 'redactor_manager';

                            $this.$modalBody.html($this.$modalBody.html());
                            $compile($this.$modalBody.contents())($this.$element.scope())

                            // set some essential scope variables
                            //$this.$modalBody.scope().redactorImageSearcher = ImageSearch('image_listing');

                            $this.$modalBody.scope().uploadSuccess = function () {
                                $this.$element.scope().redactorImageSearcher.resetCache();
                                $this.$element.scope().redactorImageSearcher.search({
                                    page: 1,
                                    _format: 'partial',
                                    include_metadata: 1,
                                    include_full_agg: 0
                                }).then(function (response) {
                                    var searchElement = $this.$modalBody.find('#redactor-image-search');
                                    searchElement.html(response.data.template);
                                    $compile(searchElement.contents())($this.$modalBody.scope())
                                });
                            };

                            $this.$modalBody.scope().select = function (path, filterable) {
                                var img = '<img src="';

                                if ('undefined' == typeof filterable) {
                                    filterable = false;
                                }

                                if (true === filterable) {
                                    img += path.replace('replaceable_filter', 'original') + '" data-src="' + path + '" data-filter="original">';
                                } else {
                                    img += path + '">';
                                }

                                $this.image.insert(img);
                            };
                        }
                    },
                    customUpdate: function ($image) {
                        // update the source
                        var filter = $('#redactor-image-size').val();

                        if (undefined != filter) {
                            $image.attr('src', $image.data('src').replace('replaceable_filter', filter));

                            $image.data('filter', filter);

                            $image.attr('data-filter', filter);
                        }

                        $this.image.update($image);
                    },
                    loadEditableControls: function ($image) {
                        var imageBox = $('<span id="redactor-image-box" data-redactor="verified">');
                        imageBox.css('float', $image.css('float')).attr('contenteditable', false);

                        if ($image[0].style.margin != 'auto') {
                            imageBox.css({
                                marginTop: $image[0].style.marginTop,
                                marginBottom: $image[0].style.marginBottom,
                                marginLeft: $image[0].style.marginLeft,
                                marginRight: $image[0].style.marginRight
                            });

                            $image.css('margin', '');
                        }
                        else {
                            imageBox.css({'display': 'block', 'margin': 'auto'});
                        }

                        $image.css('opacity', '.5').after(imageBox);

                        // editter
                        $this.image.editter = $('<span id="redactor-image-editter" data-redactor="verified">' + $this.lang.get('edit') + '</span>');
                        $this.image.editter.attr('contenteditable', false);
                        $this.image.editter.on('click', $.proxy(function () {
                            $this.image.customShowEdit($image);
                        }, $this));

                        imageBox.append($this.image.editter);

                        // position correction
                        var editerWidth = $this.image.editter.innerWidth();
                        $this.image.editter.css('margin-left', '-' + editerWidth / 2 + 'px');

                        // resizer
                        if ($this.opts.imageResizable && !$this.utils.isMobile()) {
                            var imageResizer = $('<span id="redactor-image-resizer" data-redactor="verified"></span>');

                            if (!$this.utils.isDesktop()) {
                                imageResizer.css({width: '15px', height: '15px'});
                            }

                            imageResizer.attr('contenteditable', false);
                            imageBox.append(imageResizer);
                            imageBox.append($image);

                            return imageResizer;
                        }
                        else {
                            imageBox.append($image);
                            return false;
                        }
                    },
                    customShowEdit: function ($image) {
                        $this.image.showEdit($image);

                        var filter = $image.data('filter');

                        if (undefined != filter) {
                            $('#redactor-image-size-wrapper').show();
                            $('#redactor-image-size').val($image.data('filter'));
                        } else {
                            $('#redactor-image-size-wrapper').hide();
                        }

                        $this.image.buttonSave.off('click').on('click', $.proxy(function () {
                            this.image.customUpdate($image);

                        }, $this));
                    }
                });

                var button = this.button.add('image', this.lang.get('image'));
                this.button.addCallback(button, this.image.load);
            }
        }
    };
})(jQuery);
