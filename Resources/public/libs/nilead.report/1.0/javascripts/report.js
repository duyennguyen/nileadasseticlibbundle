angular.module('nilead.module.report', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })

    .service('ReportRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'search',
            resource: 'global',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('ReportResponse', ['ResponseFactory', 'ReportRouter', function (ResponseFactory, ReportRouter) {
        return new ResponseFactory(ReportRouter);
    }])
    .service('Report', ['ResourceFactory', 'ReportRouter', 'ReportResponse', function (ResourceFactory, ReportRouter, ReportResponse) {
        return new ResourceFactory(ReportRouter, ReportResponse, function (Resource, ReportRouter, ReportResponse) {
            return {
                report: function (params, success, error) {
                    var event = this.getEvent('report');
                    return Resource.get(ReportRouter.generate('report', params),
                        function (response) {
                            return ReportResponse.success(response, success, event);
                        },
                        function (response) {
                            return ReportResponse.error(response, error, event)
                        }
                    );
                }
            }
        });
    }])
;
