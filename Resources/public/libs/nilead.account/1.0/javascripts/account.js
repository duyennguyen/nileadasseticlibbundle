angular.module('nilead.account.account', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('AccountRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'account',
            resource: 'account',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('AccountResponse', ['ResponseFactory', 'AccountRouter', function (ResponseFactory, AccountRouter) {
        return new ResponseFactory(AccountRouter);
    }])
    .service('Account', ['ResourceFactory', 'AccountRouter', 'AccountResponse', function (ResourceFactory, AccountRouter, AccountResponse) {
        return new ResourceFactory(AccountRouter, AccountResponse, function (Resource, AccountRouter, AccountResponse) {
            return {
                login: function (params, success, error) {
                    var event = this.getEvent('check');
                    return Resource.post(AccountRouter.generate('check'), params,
                        function (response) {
                            return AccountResponse.success(response, success, event);
                        },
                        function (response) {
                            return AccountResponse.error(response, error, event);
                        }
                    );
                },
                register: function (params, success, error) {
                    var event = this.getEvent('register');
                    return Resource.post(AccountRouter.generate('register'), params,
                        function (response) {
                            return AccountResponse.success(response, success, event);
                        },
                        function (response) {
                            return AccountResponse.error(response, error, event);
                        }
                    );
                },
                setting: function (data, success, error) {
                    var event = this.getEvent('setting');
                    return Resource.post(AccountRouter.generate('setting'), data,
                        function (response) {
                            return AccountResponse.success(response, success, event);
                        }, function (response) {
                            return AccountResponse.error(response, error, event);
                        }
                    );
                },
                changePassword: function (data, success, error) {
                    var event = this.getEvent('change_password');
                    return Resource.post(AccountRouter.generate('change_password'), data,
                        function (response) {
                            return AccountResponse.success(response, success, event);
                        }, function (response) {
                            return AccountResponse.error(response, error, event);
                        }
                    );
                },
                createMember: function (data, success, error) {
                    var event = this.getEvent('create_member');
                    return Resource.post(AccountRouter.generate('create_member'), data,
                        function (response) {
                            return AccountResponse.success(response, success, event);
                        }, function (response) {
                            return AccountResponse.error(response, error, event);
                        }
                    );
                },
                updateMember: function (id, data, success, error) {
                    var event = this.getEvent('update_member');

                    return Resource.post(AccountRouter.generate('update_member', {id: id}), data,
                        function(response) {
                            return AccountResponse.success(response, success, event);
                        },
                        function(response) {
                            return AccountResponse.error(response, error, event);
                        }
                    );
                }
            }
        });
    }])
    .service('AccountBulk', ['Account', 'BulkFactory', function (Account, BulkFactory) {
        return new BulkFactory(Account, {
            actions: {
                enable: null,
                disable: null
            }
        });
    }])
    .service('AccountSearch', ['AccountRouter', 'SearchFactory', function (AccountRouter, SearchFactory) {

        return function (id, preset, action) {
            if (!angular.isDefined(action)) {
                action = 'index';
            }

            return SearchFactory(id, {
                action: action,
                router: AccountRouter,
                entities: [AccountRouter.getResource()]
            },'account', preset);
        }
    }])
;
