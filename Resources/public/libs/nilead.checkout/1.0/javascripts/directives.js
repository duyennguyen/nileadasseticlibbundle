angular.module('nilead.checkout.checkout')
    .directive('nlSameAddress', function ($parse) {
        return {
            restrict: 'A',
            priority: 1000,
            link: function (scope, element, attrs) {
                var getterFrom = $parse(attrs.nlFrom);
                var setterTo = $parse(attrs.nlTo).assign;

                var getterFromExtra = $parse(attrs.nlFromExtra);
                var setterToExtra = $parse(attrs.nlToExtra).assign;

                scope.$watch(attrs.ngModel, function (checked) {
                    if (checked) {
                        setterTo(scope, getterFrom(scope));
                        setterToExtra(scope, getterFromExtra(scope));
                    } else {
                        setterTo(scope, angular.copy(getterFrom(scope)));
                        setterToExtra(scope, angular.copy(getterFromExtra(scope)));
                    }
                });
            }
        }
    })
    .directive('nlRemoveDiscount',['Checkout', '$parse', '$compile', function(Checkout, $parse, $compile){
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('click', function(e) {

                    var target = angular.element(attrs.nlTarget);
                    Checkout.removeOrderAdjustment(attrs.nlClientKey, attrs.nlAdjustment, function (response) {
                        if (undefined !== response.data.template) {
                            target.html(response.data.template);
                            $compile(target.contents())(scope);
                        }
                        return false;
                    });
                })
            }
        }
    }])

    .directive('nlAddDiscount',['Checkout', '$parse', '$compile', function(Checkout, $parse, $compile){
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('click', function(e) {
                    var target = angular.element(attrs.nlTarget);
                    var form = $parse(attrs.nlModel)(scope);
                    Checkout.addDiscountCode(attrs.nlClientKey, form, function (response) {
                        if (undefined !== response.data.template) {
                            target.html(response.data.template);
                            $compile(target.contents())(scope);
                        }
                        return false;
                    });
                })
            }
        }
    }]);
