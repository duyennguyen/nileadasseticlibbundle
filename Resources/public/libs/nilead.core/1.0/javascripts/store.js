angular.module('nilead.core.store', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('StoreRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'core',
            resource: 'store',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('StoreResponse', ['ResponseFactory', 'StoreRouter', function (ResponseFactory, StoreRouter) {
        return new ResponseFactory(StoreRouter);
    }])
    .service('Store', ['ResourceFactory', 'StoreRouter', 'StoreResponse', function (ResourceFactory, StoreRouter, StoreResponse) {
        return new ResourceFactory(StoreRouter, StoreResponse);
    }])
    .service('StoreBulk', ['Store', 'BulkFactory', function (Store, BulkFactory) {
        return new BulkFactory(Store, {
            actions: {
                delete: null
            }
        });
    }])
;
