angular.module('nilead.common')
    .service('RouteGenerator', function () {
        return {
            generateRoute: function(options){
                var route = '';

                route += options.app + '.';
                route += options.bundle + '.';
                route += options.env + '.';

                // sometimes we may not have resource
                var resource = options.resource;
                if (resource) {
                    route += resource + '.';
                }

                route += options.action;

                return route;
            },
            generateState: function(options){
                var route = '';

                route += options.bundle + '_';
                route += options.env + '_';

                // sometimes we may not have resource
                var resource = options.resource;
                if (resource) {
                    route += resource + '.';
                }

                route +=  options.action;

                return route;
            }
        }
    })
    .factory('RouterFactory', ['Environment', 'RouteGenerator', function (Environment, RouteGenerator) {
        var RouterFactory = function (options) {
            var currentParams = options.params || {};

            /**
             * Naming strategy helper, build based on ResourceNamingStrategy
             *
             * @param object options
             *
             * @return namingStrategy
             */
            var namingStrategy = function (options) {
                options.app      = options.app || null;
                options.bundle   = options.bundle || null;
                options.resource = options.resource || null;

                return {
                    /**
                     * Gets the application name
                     *
                     * @return string
                     */
                    getApp: function () {
                        return options.app;
                    },

                    /**
                     * Gets the bundle alias
                     *
                     * @return string
                     */
                    getBundle: function () {
                        return options.bundle;
                    },

                    /**
                     * Gets the resource name
                     *
                     * @return string
                     */
                    getResource: function () {
                        return options.resource;
                    },

                    /**
                     * Gets the current action of this resource
                     *
                     * @return string
                     */
                    getAction: function () {
                        return options.action;
                    },

                    /**
                     * Gets the route name by the given action
                     *
                     * @param string action
                     *
                     * @return string
                     */
                    getRoute: function (action) {
                        options.action = action;

                        return RouteGenerator.generateRoute(angular.extend({action: action, env: Environment.get()}, options));
                    },
                    getState: function (action) {
                        options.action = action;

                        return RouteGenerator.generateState(angular.extend({action: action, env: Environment.get()}, options));
                    }

                }
            }(options);


            return {
                /**
                 * Get the route name for given action
                 *
                 * @param string action
                 *
                 * @return string
                 */
                getRoute: function (action) {
                    return namingStrategy.getRoute(action);
                },

                /**
                 * Generate the routing URL by using the given action and params
                 *
                 * @param string action
                 * @param object params
                 *
                 * @return string
                 *
                 * @throws RouteNotFoundException If the route does not exist
                 */
                generate: function (action, params) {
                    currentParams = angular.extend({}, options.params || {_format: 'json'}, 'object' === typeof params ? params : {});

                    try {
                        return Routing.generate(namingStrategy.getRoute(action), currentParams);
                    } catch (error) {
                        console.error('RouteNotFoundException: ' + error.message);
                    }
                },

                /**
                 * Gets the resource name registered to naming strategy helper
                 *
                 * @return string
                 */
                getResource: function () {
                    return namingStrategy.getResource();
                },

                /**
                 * Gets the last action generated by this router
                 *
                 * @return string
                 */
                getAction: function () {
                    return namingStrategy.getAction();
                },

                /**
                 * Gets the format name from last route generated
                 *
                 * @return string
                 */
                getFormat: function () {
                    return currentParams._format;
                },

                /**
                 * Gets angular state name by the given action
                 *
                 * @param string|null name
                 *
                 * @return string
                 */
                getState: function (name) {
                    return this.getResource() + '_' + (name || this.getAction());
                }
            }
        };

        /**
         * Factory method
         *
         * @param object options
         *
         * @return RouterFactory
         */
        return function (options) {
            return new RouterFactory(options);
        }
    }])
;
