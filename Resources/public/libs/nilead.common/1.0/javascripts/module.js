angular.module('nilead.common', ['restangular', 'angular-growl', 'ngAnimate'])
    .config(['$interpolateProvider', 'RestangularProvider', 'growlProvider', function ($interpolateProvider, RestangularProvider, growlProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}');

        growlProvider.globalTimeToLive(5000);
        growlProvider.globalReversedOrder(true);
        growlProvider.globalDisableCountDown(true);

        return RestangularProvider.setDefaultHeaders({
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest'
        });
    }])
;
