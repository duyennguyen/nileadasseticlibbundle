angular.module('nilead.theme.theme', ['nilead.common', 'blockUI'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('ThemeRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'theme',
            resource: 'theme',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('ThemeResponse', ['ResponseFactory', 'ThemeRouter', function (ResponseFactory, ThemeRouter) {
        return new ResponseFactory(ThemeRouter);
    }])
    .service('Theme', ['ResourceFactory', 'ThemeRouter', 'ThemeResponse', 'Resource', function (ResourceFactory, ThemeRouter, ThemeResponse, Resource) {
        return new ResourceFactory(ThemeRouter, ThemeResponse, function(Resource, ThemeRouter, ThemeResponse){
            return {
                getThemes: function(success, error) {
                    var event = this.getEvent('get_themes');
                    return Resource.get(Routing.generate('nilead.api.theme.index'),
                        function (response) {
                            return ThemeResponse.success(response, success, event);
                        },
                        function (response) {
                            return ThemeResponse.error(response, error, event);
                        }
                    );
                },
                updateSessionThemeSettings: function (id, data, success, error) {
                    var event = this.getEvent('update_session_theme_settings');
                    return Resource.post(ThemeRouter.generate('update_session_theme_settings', {id: id}), data,
                        function (response) {
                            return ThemeResponse.success(response, success, event);
                        },
                        function (response) {
                            return ThemeResponse.error(response, error, event);
                        }
                    );
                },
                loadEditor: function (id, success, error) {
                    var event = this.getEvent('editor');
                    return Resource.get(ThemeRouter.generate('editor', {id: id }),
                        function (response) {
                            return ThemeResponse.success(response, success, event);
                        },
                        function (response) {
                            return ThemeResponse.error(response, error, event);
                        }
                    );
                },
                isInstalled: function(theme, success, error) {
                    var event = this.getEvent('is_installed');
                    return Resource.get(ThemeRouter.generate('is_installed', { theme: theme }),
                        function (response) {
                            return ThemeResponse.success(response, success, event);
                        },
                        function (response) {
                            return ThemeResponse.error(response, error, event);
                        }
                    );
                },
                store: function(success, error) {
                    var event = this.getEvent('store');
                    return Resource.get(ThemeRouter.generate('store'),
                        function (response) {
                            return ThemeResponse.success(response, success, event);
                        },
                        function (response) {
                            return ThemeResponse.error(response, error, event);
                        }
                    );
                },
                manager: function(success, error) {
                    var event = this.getEvent('manager');
                    return Resource.get(ThemeRouter.generate('manager'),
                        function (response) {
                            return ThemeResponse.success(response, success, event);
                        },
                        function (response) {
                            return ThemeResponse.error(response, error, event);
                        }
                    );
                },
                setDefaultTheme: function(device, theme, success, error) {
                    var event = this.getEvent('set_default_theme');
                    return Resource.get(ThemeRouter.generate('set_default_theme', { device: device, themeId: theme }),
                        function (response) {
                            return ThemeResponse.success(response, success, event);
                        },
                        function (response) {
                            return ThemeResponse.error(response, error, event);
                        }
                    );
                },
                install: function(app, theme, success, error) {
                    var event = this.getEvent('install');
                    return Resource.get(ThemeRouter.generate('install', { app: app, theme: theme }),
                        function (response) {
                            return ThemeResponse.success(response, success, event);
                        },
                        function (response) {
                            return ThemeResponse.error(response, error, event);
                        }
                    );
                },
                uninstall: function(app, theme, success, error) {
                    var event = this.getEvent('uninstall');
                    return Resource.get(ThemeRouter.generate('uninstall', { app: app, theme: theme }),
                        function (response) {
                            return ThemeResponse.success(response, success, event);
                        },
                        function (response) {
                            return ThemeResponse.error(response, error, event);
                        }
                    );
                },
                toggleAutoUpdate: function(id, success, error) {
                    var event = this.getEvent('toggleAutoUpdate');
                    return Resource.get(ThemeRouter.generate('toggle_auto_update', { id: id }),
                        function (response) {
                            return ThemeResponse.success(response, success, event);
                        },
                        function (response) {
                            return ThemeResponse.error(response, error, event);
                        }
                    );
                },
                upload: function(id, success, error) {
                    var event = this.getEvent('upload');
                    return Resource.get(ThemeRouter.generate('upload'),
                        function (response) {
                            return ThemeResponse.success(response, success, event);
                        },
                        function (response) {
                            return ThemeResponse.error(response, error, event);
                        }
                    );
                },
                listContents: function (id, data, success, error) {
                    var event = this.getEvent('list_contents');
                    return Resource.get(ThemeRouter.generate('list_contents', {id: id}),
                        function (response) {
                            return ThemeResponse.success(response, success, event);
                        },
                        function (response) {
                            return ThemeResponse.error(response, error, event);
                        }
                    );
                },
                changePreset: function (id, presetName, success, error) {
                    var event = this.getEvent('change_preset');
                    return Resource.get(ThemeRouter.generate('change_preset', {id: id, preset : presetName}),

                        function (response) {
                            return ThemeResponse.success(response, success, event);
                        },
                        function (response) {
                            return ThemeResponse.error(response, error, event);
                        }
                    );
                }
            }
        });
    }])
    .service('ThemeBulk', ['Theme', 'BulkFactory', function (Theme, BulkFactory) {
        return new BulkFactory(Theme, {
            actions: {
                active: null
            }
        });
    }])
    .directive('nlThemeStore', ['$compile', 'Theme', function ($compile, Theme) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                scope.themes = {};
            }
        }
    }])
    .directive('nlSetDefaultTheme', ['$compile', 'Theme', function ($compile, Theme) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('click', function () {
                    Theme.setDefaultTheme(attrs.nlDevice, attrs.nlTheme);
                });
            }
        };
    }])
    .directive('nlThemeInstall', ['$compile', 'Theme', '$interval', '$timeout', 'blockUI', 'growl', function ($compile, Theme, $interval, $timeout, blockUI, growl) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('click', function () {
                    Theme.install(attrs.nlApp, attrs.nlTheme, function (response){
                        blockUI.start();
                        $timeout(function(){
                            var checkedTheme = $interval(function(){
                                Theme.isInstalled(attrs.nlTheme, false, false).then(function (response){
                                    // redirect to theme manager
                                    if (undefined == response.data.installed) {
                                        $interval.cancel(checkedTheme);
                                        blockUI.stop();
                                    }
                                    if (response.data.installed == false) {
                                        return false;
                                    }
                                })
                            }, 10000);
                        }, 5000);

                        return false;
                    })
                });
            }
        };
    }])
    .directive('nlThemeEditor', ['Theme', 'ThemeFile', 'EventListener', function (Theme, ThemeFile, EventListener) {
        return {
            scope: {
                nlId: '@',
                nlThemeEditor: '@'
            },
            replace: true,
            templateUrl: 'editor.html',
            controller: ['$scope', '$parse', function ($scope, $parse) {
                $scope.$editor = {
                    editor: {},
                    current: {
                        contents: '',
                        key: '',
                        fileName: '',
                        fileType: '',
                        revisions: {}
                    },
                    files: {}
                };

                var getter = $parse($scope.nlThemeEditor);
                var setter = getter.assign;

                $scope.aceLoaded = function (_editor) {
                    $scope.$editor.editor = _editor;
                };

                var loadResponse = function (key, fileName, fileType, response) {
                    $scope.$editor.files[key] = {
                        key: key,
                        fileName: fileName,
                        fileType: fileType,
                        contents: response.data.data.theme_file.contents,
                        revisions: response.data.data.revisions
                    };
                };

                var renderFile = function (file) {
                    setter($scope, file.contents);

                    $scope.$editor.current = file;

                    $scope.$editor.editor.getSession().setMode('ace/mode/' + file.fileType);

                    EventListener.emit('theme_editor_load_file_' + $scope.nlId);
                };

                $scope.aceChanged = function(e) {
                    $scope.$editor.current.contents = getter($scope);
                };

                $scope.loadRevision = function () {
                    if ($scope.$editor.current.revisions.hasOwnProperty($scope.revision)) {
                        setter($scope, $scope.$editor.current.revisions[$scope.revision].data.contents);
                    }
                };

                $scope.save = function (form) {
                    ThemeFile.update($scope.nlId, $scope.$editor.current.key, form, function (response) {
                        loadResponse($scope.$editor.current.key, $scope.$editor.current.fileName, $scope.$editor.current.fileType, response);

                        renderFile($scope.$editor.files[$scope.$editor.current.key]);
                    });
                };

                $scope.renderFile = function (key) {
                    if ($scope.$editor.files.hasOwnProperty(key)) {
                        renderFile($scope.$editor.files[key]);
                    }
                };

                $scope.loadFile = function (path, fileType, $event) {
                    ThemeFile.show($scope.nlId, path, {}, function (response) {
                        $scope.$editor.files[$scope.$editor.current.key] = $scope.$editor.current;

                        loadResponse(path, angular.element($event.currentTarget).text(), fileType, response);
                        // assign current if any to the storage

                        renderFile($scope.$editor.files[path]);

                        return false;
                    }, function (response) {

                        return false;
                    });
                };

                Theme.listContents($scope.nlId, {}, function (response) {
                    EventListener.emit('theme_editor_load_contents_' + $scope.nlId, response.data.template);

                    return false;
                });
            }]
        };
    }])
    .directive('nlThemeEditorFiles', ['Theme', 'EventListener', '$compile', function (Theme, EventListener, $compile) {
        return {
            require: '^nlThemeEditor',
            link: function (scope, element, attr, Ctrl) {
                EventListener.on('theme_editor_load_contents_' + scope.nlId, function(template) {

                    element.html(template);
                    $compile(element.contents())(scope);
                });
            }
        }
    }])
    .directive('nlThemeEditorRevisions', ['EventListener', '$timeout', function (EventListener, $timeout) {
        return {
            require: '^nlThemeEditor',
            link: function (scope, element, attr, Ctrl) {
                EventListener.on('theme_editor_load_file_' + scope.nlId, function() {

                    $timeout(function () {
                        element.select2({});
                    });
                });
            }
        }
    }])
    .directive('nlThemePreview', ['$window', '$parse', 'Theme', 'nlHistory', function ($window, $parse, Theme, nlHistory) {
        return {
            templateUrl: 'theme_preview.html',
            link: function (scope, element, attrs) {

                scope.history = nlHistory.decorate(attrs.nlModel, scope);

                var getter = $parse(attrs.nlForm);

                var iframe = element.find('iframe')[0];

                element.find('h4.panel-title > a').click(function(event) {
                    event.preventDefault();
                });

                $window.addEventListener("message", function(event) {
                    var data = JSON.parse(event.data);

                    if (data.hasOwnProperty('blocks')) {
                        scope.blocks = data.blocks;
                        scope.$apply();
                    }
                }, false);

                var updateSessionThemeSettings = function (revision) {
                    Theme.updateSessionThemeSettings(attrs.nlThemeId, getter(scope), function() {

                        iframe.contentWindow.postMessage('reload', '*');

                        return false;
                    });
                };

                scope.history.addPersistCallback(function(revision) {
                    if (revision > 0) {
                        updateSessionThemeSettings(revision);
                    }
                });

                scope.history.addRevertCallback(function(revision) {
                    updateSessionThemeSettings(revision);
                });

                scope.switchMode = function (mode) {
                    if(element[0].hasAttribute('data-nl-theme-preview')) {
                        element.attr('data-nl-theme-preview', mode);
                    }
                    else {
                        element.attr('nl-theme-preview', mode);
                    }
                };
            }
        }
    }]);
