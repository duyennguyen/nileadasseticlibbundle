angular.module('nilead.common')
    .provider('Environment', function () {
        var env = 'frontend';
        return {
            init: function($env){
                env = $env;
            },
            $get: function() {
                return {
                    set: function ($env) {
                        env = $env;
                    },
                    get: function () {
                        return env;
                    }
                }
            }
        }
    })
;
