angular.module('nilead.content.content', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('ContentRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'content',
            resource: 'content',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('ContentResponse', ['ResponseFactory', 'ImageAwareResponseFactory', 'ContentRouter', function (ResponseFactory, ImageAwareResponseFactory, ContentRouter) {
        return ImageAwareResponseFactory(new ResponseFactory(ContentRouter));
    }])
    .service('Content', ['ResourceFactory', 'ContentRouter', 'ContentResponse', function (ResourceFactory, ContentRouter, ContentResponse) {
        return new ResourceFactory(ContentRouter, ContentResponse);
    }])
    .service('ContentBulk', ['Content', 'BulkFactory', function (Content, BulkFactory) {
        return new BulkFactory(Content, {
            actions: {
                enable: null,
                disable: null,
                delete: null
            }
        });
    }])

    .service('ContentSearch', ['ContentRouter', 'SearchFactory', function (ContentRouter, SearchFactory) {
        return function (id, preset, action) {
            if ( ! angular.isDefined(action)) {
                action = 'index';
            }

            return SearchFactory(id, {
                action: action,
                router: ContentRouter,
                entities: [ContentRouter.getResource()]
            }, 'content', preset);
        }
    }])
;
