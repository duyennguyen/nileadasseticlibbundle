angular.module('nilead.checkout.checkin', ['nilead.common', 'nilead.checkout.checkout'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('CheckinRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'checkout',
            resource: 'checkin',
            params: {
                _format: 'partial'
            }
        });
    }])

    .service('CheckinResponse', ['ResponseFactory', 'CheckinRouter', function (ResponseFactory, CheckinRouter) {
        return new ResponseFactory(CheckinRouter);
    }])
    .service('Checkin', ['ResourceFactory', 'CheckinRouter', 'CheckinResponse', 'CheckoutCommon', function (ResourceFactory, CheckinRouter, CheckinResponse, CheckoutCommon) {
        return new ResourceFactory(CheckinRouter, CheckinResponse, CheckoutCommon);
    }])

    .factory('backendCheckin', function () {
        var items = {};
        var total = 0;

        return {
            init: function (order) {

            }, removeItem: function (item) {
                item.quantity = 0;
                this.updateItem(item);
            }, addItem: function (id, description, price) {
                if (items.hasOwnProperty(id)) {
                    items[id].quantity += 1;
                } else {
                    items[id] = {id: id, description: description, quantity: 1, unitPrice: parseInt(price)};
                }

                this.updateItem(items[id]);
            }, updateItem: function (item) {
                if (0 == item.quantity) {
                    delete items[item.id];
                } else {
                    item.total = item.quantity * item.unitPrice;
                }

                this.updateCart();
            }, getItems: function () {
                return items;
            }, getTotal: function () {
                return total;
            }, updateCart: function () {
                total = 0;
                angular.forEach(items, function (item, key) {
                    total += item.unitPrice * item.quantity;
                });
            }
        };
    });
