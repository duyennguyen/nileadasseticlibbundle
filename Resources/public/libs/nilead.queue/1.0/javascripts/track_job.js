angular.module('nilead.queue.track_job', ['nilead.common', 'blockUI'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })

    .service('TrackJobRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'queue',
            resource: 'track_job',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('TrackJobResponse', ['ResponseFactory', 'TrackJobRouter', function (ResponseFactory, TrackJobRouter) {
        return new ResponseFactory(TrackJobRouter);
    }])
    .service('TrackJob', ['ResourceFactory', 'TrackJobRouter', 'TrackJobResponse', 'Resource', function (ResourceFactory, TrackJobRouter, TrackJobResponse, Resource) {
        return new ResourceFactory(TrackJobRouter, TrackJobResponse);
    }])
    .service('TrackJobBulk', ['TrackJob', 'BulkFactory', function (TrackJob, BulkFactory) {
        return new BulkFactory(TrackJob, {
            actions: {
                delete: null
            }
        });
    }])