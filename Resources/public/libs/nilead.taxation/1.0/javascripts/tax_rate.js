angular.module('nilead.taxation.tax_rate', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('TaxRateRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'taxation',
            resource: 'tax_rate',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('TaxRateResponse', ['ResponseFactory', 'TaxRateRouter', function (ResponseFactory, TaxRateRouter) {
        return new ResponseFactory(TaxRateRouter);
    }])
    .service('TaxRate', ['ResourceFactory', 'TaxRateRouter', 'TaxRateResponse', function (ResourceFactory, TaxRateRouter, TaxRateResponse) {
        return new ResourceFactory(TaxRateRouter, TaxRateResponse);
    }])
    .service('TaxRateBulk', ['TaxRate', 'BulkFactory', function (TaxRate, BulkFactory) {
        return new BulkFactory(TaxRate, {
            actions: {
                enable: null,
                disable: null,
                delete: null
            }
        });
    }])
;
