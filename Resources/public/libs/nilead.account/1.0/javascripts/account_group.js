angular.module('nilead.account.account_group', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })

    .service('AccountGroupRouter', ['RouterFactory',
        function (RouterFactory) {
            return new RouterFactory({
                app: 'nilead',
                bundle: 'account',
                resource: 'account_group',
                params: {
                    _format: 'partial'
                }
            });
        }
    ])
    .service('AccountGroupResponse', ['ResponseFactory', 'AccountGroupRouter',
        function (ResponseFactory, AccountGroupRouter) {
            return new ResponseFactory(AccountGroupRouter);
        }
    ])
    .service('AccountGroup', ['ResourceFactory', 'AccountGroupRouter', 'AccountGroupResponse',
        function (ResourceFactory, AccountGroupRouter, AccountGroupResponse) {
            return new ResourceFactory(AccountGroupRouter, AccountGroupResponse);
        }
    ])
    .service('AccountGroupBulk', ['AccountGroup', 'BulkFactory',
        function (AccountGroup, BulkFactory) {
            return new BulkFactory(AccountGroup, {
                actions: {
                    active: null
                }
            });
        }
    ])
;
