angular.module('nilead.taxation.tax_category', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('TaxCategoryRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'taxation',
            resource: 'tax_category',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('TaxCategoryResponse', ['ResponseFactory', 'TaxCategoryRouter', function (ResponseFactory, TaxCategoryRouter) {
        return new ResponseFactory(TaxCategoryRouter);
    }])
    .service('TaxCategory', ['ResourceFactory', 'TaxCategoryRouter', 'TaxCategoryResponse', function (ResourceFactory, TaxCategoryRouter, TaxCategoryResponse) {
        return new ResourceFactory(TaxCategoryRouter, TaxCategoryResponse);
    }])
;
