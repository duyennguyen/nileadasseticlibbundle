angular.module('nilead.setting.configuration', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('ConfigurationRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'setting',
            resource: 'configuration',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('ConfigurationResponse', ['ResponseFactory', 'ConfigurationRouter', function (ResponseFactory, ConfigurationRouter) {
        return new ResponseFactory(ConfigurationRouter);
    }])
    .service('Configuration', ['ResourceFactory', 'ConfigurationRouter', 'ConfigurationResponse', 'Resource', function (ResourceFactory, ConfigurationRouter, ConfigurationResponse, Resource) {
        return new ResourceFactory(ConfigurationRouter, ConfigurationResponse, function(Resource, ConfigurationRouter, ConfigurationResponse){
            return {
                update: function(namespace, template, data, success, error) {
                    var event = this.getEvent('update');
                    return Resource.post(ConfigurationRouter.generate('update', { namespace: namespace, template: template, _format: 'partial' }), data,
                        function (response) {
                            return ConfigurationResponse.success(response, success, event);
                        },
                        function (response) {
                            return ConfigurationResponse.error(response, error, event);
                        });
                }
            }
        });
    }])
;
