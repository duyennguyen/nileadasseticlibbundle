/**
 * Created by Rubikin Team.
 * ========================
 * Date: 2013-08-15
 * Time: 11:39:23 PM
 *
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

(function ($) {
    'use strict';

    $.fn.formCollection = function (settings) {
        if ('object' !== typeof settings) {
            settings = {};
        }

        settings = {
            button:  settings.button  || '[data-collection-button]',
            method:  settings.method  || 'collection-button',
            holder:  settings.holder  || 'holder',
            element: settings.element || 'element',
            prototypeName: settings.prototypeName || '__name__',

            callback: {
                global: {
                    process: {
                        pre: 'onFormCollectionPreProcess',
                        post: 'onFormCollectionPostProcess'
                    },

                    add: {
                        pre: 'onFormCollectionPreAdd',
                        post: 'onFormCollectionPostAdd'
                    },

                    delete: {
                        pre: 'onFormCollectionPreDelete',
                        post: 'onFormCollectionPostDelete'
                    }
                },

                local: {
                    process: {
                        pre: settings.onPreProcess || null,
                        post: settings.onPostProcess || null
                    },

                    add: {
                        pre: settings.onPreAdd || null,
                        post: settings.onPostAdd || null
                    },

                    delete: {
                        pre: settings.onPreDelete || null,
                        post: settings.onPostDelete || null
                    }
                }
            }
        };

        this.each(function (event) {
            $(this).on('click', settings.button, function (event) {
                dispatchGlobalEvent(settings.callback.global.process.pre, { event: event });

                event.preventDefault();

                var method = $(this).data(settings.method).toLowerCase();

                if ('add' === method) {
                    onAddCollectionButtonClick($(this));
                }

                if ('delete' === method) {
                    onDeleteCollectionButtonClick($(this));
                }

                dispatchGlobalEvent(settings.callback.global.process.post, { event: event });
            });
        });

        /**
         * Process add element
         *
         * @param object element
         *
         * @return void
         */
        var onAddCollectionButtonClick = function (element) {
            var item;
            var holder = element.data(settings.holder);

            if ('function' === typeof settings.holder) {
                holder = settings.holder(element);
            } else if ('function' === typeof window[holder]) {
                holder = window[holder](element);
            } else {
                holder = $(holder);
            }

            var index = holder.data('index') || holder.children().length;
            var prototype = element.data('prototype');

            if ('function' === typeof window[prototype]) {
                prototype = window[prototype](element);
            }

            var prototypeName = element.data('prototype-name') || settings.prototypeName;
            var pattern = new RegExp(prototypeName, 'g');

            try {
                item = $(prototype.replace(pattern, index));
            } catch (e) {
                item = $($.parseHTML(prototype.replace(pattern, index))[0].data);
            }

            // Trigger global callback function if it's defined
            dispatchGlobalEvent(settings.callback.global.add.pre, { holder: holder, item: item });
            dispatchLocalEvent(settings.callback.local.add.pre, { holder: holder, item: item });

            // Add item to holder and increase the index value
            holder.append(item);
            holder.data('index', ++index);

            // Trigger global callback function if it's defined
            dispatchGlobalEvent(settings.callback.global.add.post, { holder: holder, item: item });
            dispatchLocalEvent(settings.callback.local.add.post, { holder: holder, item: item });
        };

        /**
         * Process delete element
         *
         * @param object element
         *
         * @return void
         */
        var onDeleteCollectionButtonClick = function (element) {
            var removeElement = element.data(settings.element);

            if ('function' === typeof window[removeElement]) {
                removeElement = window[removeElement](element);
            } else {
                removeElement = element.closest(removeElement);
            }

            // Trigger callback function if it's defined
            dispatchGlobalEvent(settings.callback.global.delete.pre, { element: removeElement });
            dispatchLocalEvent(settings.callback.local.delete.pre, { element: removeElement });

            // Remove removeElement from holder
            removeElement.remove();

            // Trigger callback function if it's defined
            dispatchGlobalEvent(settings.callback.global.delete.post, { element: removeElement });
            dispatchLocalEvent(settings.callback.local.delete.post, { element: removeElement });
        };

        /**
         * Dispatch global events if it's defined
         *
         * @param object event
         * @param object args
         *
         * @return void
         */
        var dispatchGlobalEvent = function (event, args) {
            if ('function' === typeof window[event]) {
                window[event](args);
            }
        };

        /**
         * Dispatch local events if it's defined
         *
         * @param object event
         * @param object args
         *
         * @return void
         */
        var dispatchLocalEvent = function (event, args) {
            if ('function' === typeof event) {
                event(args);
            }
        };
    };
})(jQuery);
