angular.module('nilead.product.product', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('ProductRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'product',
            resource: 'product',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('ProductResponse', ['ResponseFactory', 'ImageAwareResponseFactory', 'ProductRouter', function (ResponseFactory, ImageAwareResponseFactory, ProductRouter) {
        return ImageAwareResponseFactory(new ResponseFactory(ProductRouter), function (response, resource) {
            var path = ['data', 'data', resource, 'variant', 'variants'], images = {};
            var masterPath = ['data', 'data', resource, 'variant', 'images'];

            if (_.hasPath(response, path)) {
                angular.forEach(_.getPath(response, path), function (variant) {
                    angular.forEach(variant.images, function (image) {
                        images[image.id] = image;
                    });
                });
            }

            if (_.hasPath(response, masterPath)) {
                angular.forEach(_.getPath(response, masterPath), function (image) {
                    images[image.id] = image;
                });
            }

            return images;
        });
    }])
    .service('Product', ['ResourceFactory', 'ProductRouter', 'ProductResponse', function (ResourceFactory, ProductRouter, ProductResponse) {
        return new ResourceFactory(ProductRouter, ProductResponse);
    }])
    .service('ProductBulk', ['Product', 'BulkFactory', function (Product, BulkFactory) {
        return new BulkFactory(Product, {
            actions: {
                enable: null,
                disable: null,
                delete: null
            }
        });
    }])
    .service('ProductSearch', ['ProductRouter', 'SearchFactory', function (ProductRouter, SearchFactory) {
        return function (id, preset, action) {
            if (!angular.isDefined(action)) {
                action = 'index';
            }

            return SearchFactory(id, {
                action: action,
                router: ProductRouter,
                entities: [ProductRouter.getResource()]
            }, 'product', preset);
        }
    }])
;
