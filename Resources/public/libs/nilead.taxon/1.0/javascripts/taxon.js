angular.module('nilead.taxon.taxon', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })

    .service('TaxonRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'taxon',
            resource: 'taxon',
            params: {
                _format: 'partial'
            }
        });
    }])

    .service('TaxonResponse', ['ResponseFactory', 'TaxonRouter', 'ImageAwareResponseFactory', function (ResponseFactory, TaxonRouter, ImageAwareResponseFactory) {
        return ImageAwareResponseFactory(new ResponseFactory(TaxonRouter), function (response, resource) {
            var path = ['data', 'data', resource], images = {};

            function processImages (taxon) {
                angular.forEach(taxon.images, function(image) {
                    images[image.id] = image;
                });

                if ('undefined' !== typeof taxon.children && taxon.children.length > 0) {
                    angular.forEach(taxon.children, function(child) {
                        processImages(child);
                    });
                }
            }

            if (_.hasPath(response, path)) {
                processImages(_.getPath(response, path));
            }

            return images;
        });
    }])

    .service('Taxon', ['ResourceFactory', 'TaxonRouter', 'TaxonResponse', function (ResourceFactory, TaxonRouter, TaxonResponse) {
        return new ResourceFactory(TaxonRouter, TaxonResponse);
    }])

    .service('TaxonBulk', ['Taxon', 'BulkFactory', function (Taxon, BulkFactory) {
        return new BulkFactory(Taxon, {
            actions: {
                active: null
            }
        });
    }])
;
