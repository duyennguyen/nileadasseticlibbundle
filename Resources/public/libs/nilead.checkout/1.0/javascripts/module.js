angular.module('nilead.checkout.checkout', ['nilead.common', 'nilead.account.account'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    });
