angular.module('nilead.ticket.ticket_comment', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('TicketCommentRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'ticket',
            resource: 'ticket_comment',
            params: {
                _format: 'partial'
            }
        });
    }])

    .service('TicketCommentResponse', ['ResponseFactory', 'ImageAwareResponseFactory', 'TicketCommentRouter', function (ResponseFactory, ImageAwareResponseFactory, TicketCommentRouter) {
        return ImageAwareResponseFactory(new ResponseFactory(TicketCommentRouter));
    }])

    .service('TicketComment', ['ResourceFactory', 'TicketCommentRouter', 'TicketCommentResponse', function (ResourceFactory, TicketCommentRouter, TicketCommentResponse) {
        return new ResourceFactory(TicketCommentRouter, TicketCommentResponse, function(Resource, TicketCommentRouter, TicketCommentResponse){
            return {
                create: function(id, data, success, error) {
                    var event = this.getEvent('create');
                    return Resource.post(TicketCommentRouter.generate('create', { id: id, _format: 'json' }), data,
                        function (response) {
                            return TicketCommentResponse.success(response, success, event);
                        },
                        function (response) {
                            return TicketCommentResponse.error(response, error, event);
                        });
                }
            };
        });

    }])
;
