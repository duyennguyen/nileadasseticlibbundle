angular.module('nilead.notification.hook', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('HookRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'notification',
            resource: 'hook',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('HookResponse', ['ResponseFactory', 'HookRouter', function (ResponseFactory, HookRouter) {
        return new ResponseFactory(HookRouter);
    }])
    .service('Hook', ['ResourceFactory', 'HookRouter', 'HookResponse', function (ResourceFactory, HookRouter, HookResponse) {
        return new ResourceFactory(HookRouter, HookResponse);
    }])
    .service('HookBulk', ['Hook', 'BulkFactory', function (Hook, BulkFactory) {
        return new BulkFactory(Hook, {
            actions: {
                enable: null,
                disable: null,
                delete: null
            }
        });
    }])
;
