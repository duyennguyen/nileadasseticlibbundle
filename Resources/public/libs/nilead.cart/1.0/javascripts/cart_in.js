angular.module('nilead.cart.cart_in', ['nilead.common', 'nilead.cart.cart'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })

    .service('CartInRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'cart',
            resource: 'cartin',
            params: {
                _format: 'partial'
            }
        });
    }])

    .service('CartInResponse', ['ResponseFactory', 'CartInRouter', function (ResponseFactory, CartInRouter) {
        return new ResponseFactory(CartInRouter);
    }])
    .service('CartIn', ['ResourceFactory', 'CartInRouter', 'CartInResponse', 'CartCommon', function (ResourceFactory, CartInRouter, CartInResponse, CartCommon) {
        return new ResourceFactory(CartInRouter, CartInResponse, CartCommon);
    }])
;