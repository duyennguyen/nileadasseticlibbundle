angular.module('nilead.shipment.shipping_carrier', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('ShippingCarrierRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'shipment',
            resource: 'shipping_carrier',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('ShippingCarrierResponse', ['ResponseFactory', 'ShippingCarrierRouter', function (ResponseFactory, ShippingCarrierRouter) {
        return new ResponseFactory(ShippingCarrierRouter);
    }])
    .service('ShippingCarrier', ['ResourceFactory', 'ShippingCarrierRouter', 'ShippingCarrierResponse', function (ResourceFactory, ShippingCarrierRouter, ShippingCarrierResponse) {
        return new ResourceFactory(ShippingCarrierRouter, ShippingCarrierResponse);
    }])
;
