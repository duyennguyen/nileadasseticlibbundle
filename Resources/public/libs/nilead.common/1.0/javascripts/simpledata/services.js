angular.module('nilead.common')
    .provider('SimpleData', function () {
        this.$get = [function () {
            var data = {};

            return {
                set: function (key, value) {
                    data[key] = value;
                },

                get: function (key, defaultValue) {
                    return data.hasOwnProperty(key) ? data[key] : defaultValue;
                },

                has: function (key) {
                    return data.hasOwnProperty(key);
                },

                reset: function () {
                    data = {};
                }
            }
        }]
    })
;
