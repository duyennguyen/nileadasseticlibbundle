angular.module('nilead.shipment.shipment', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('ShipmentRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'shipment',
            resource: 'shipment',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('ShipmentResponse', ['ResponseFactory', 'ShipmentRouter', function (ResponseFactory, ShipmentRouter) {
        return new ResponseFactory(ShipmentRouter);
    }])
    .service('Shipment', ['ResourceFactory', 'ShipmentRouter', 'ShipmentResponse', 'Resource', function (ResourceFactory, ShipmentRouter, ShipmentResponse, Resource) {
        return new ResourceFactory(ShipmentRouter, ShipmentResponse, function(Resource, ShipmentRouter, ShipmentResponse){
            return {
                updateSummary: function(id, summary, success, error) {
                    var event = this.getEvent('update_summary');
                    return Resource.post(ShipmentRouter.generate('update_summary', { id: id, _format: 'json' }), summary,
                        function (response) {
                            return ShipmentResponse.success(response, success, event);
                        },
                        function (response) {
                            return ShipmentResponse.error(response, error, event);
                        });
                },
                create: function (params, data, success, error) {
                    var event = this.getEvent('create');
                    return Resource.post(ShipmentRouter.generate('create', params), data,
                        function(response) {
                            return ShipmentResponse.success(response, success, event);
                        },
                        function(response) {
                            return ShipmentResponse.error(response, error, event);
                        }
                    );
                }
            };
        });
    }])
    .service('ShipmentBulk', ['Shipment', 'BulkFactory', function (Shipment, BulkFactory) {
        return new BulkFactory(Shipment, {
            actions: {
                active: null
            }
        });
    }])
    .service('ShipmentItemHandler', ['$rootScope', 'Shipment', function ($rootScope, Shipment) {
        var ShipmentItemHandler = function (Shipment) {

            /**
             * @type {Array}
             */
            var items = [];

            /**
             * @type {Array}
             */
            var itemIds = [];

            /**
             * @type {Array}
             */
            var selectedItems = [];

            /**
             * @type {Array}
             */
            var selectedIds = [];

            /**
             * @type {Array}
             */
            var instances = [];

            return ({
                createInstance: function (name) {
                    if (undefined !== name) {
                        name = Shipment.getRouter().getResource() + '.' + name;

                        if (undefined === instances[name]) {
                            instances[name] = new ShipmentItemHandler(Shipment);
                        } else {
                            instances[name].reset();
                        }

                        return instances[name];
                    } else {
                        return new ShipmentItemHandler(Shipment);
                    }
                },
                reset: function () {
                    items = [];
                    itemIds = [];
                    selectedItems = [];
                    selectedIds = [];
                },
                addItem: function (item, selected) {
                    if (_.indexOf(itemIds, item.id) == -1){
                        items.push(item);
                        itemIds.push(item.id);
                        if (selected) {
                            selectedItems.push(item);
                            selectedIds.push(item.id);
                        }
                    }
                },
                getSelectedItems: function () {
                    return selectedItems;
                },
                getItems: function () {
                    return items;
                },
                isSelected: function (id) {
                    return -1 !== _.indexOf(selectedIds, id);
                },
                toggleItem: function (id) {
                    var index = _.indexOf(selectedIds, id);
                    if (-1 === index) {
                        selectedItems.push(items[_.indexOf(itemIds, id)]);
                        selectedIds.push(id);
                    } else {
                        selectedItems.splice(index, 1);
                        selectedIds.splice(index, 1);
                    }
                }
            });
        };

        return new ShipmentItemHandler(Shipment);
    }])
;
