angular.module('nilead.product.property', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('PropertyRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'product',
            resource: 'property',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('PropertyResponse', ['ResponseFactory', 'PropertyRouter', function (ResponseFactory, PropertyRouter) {
        return new ResponseFactory(PropertyRouter);
    }])
    .service('Property', ['ResourceFactory', 'PropertyRouter', 'PropertyResponse', function (ResourceFactory, PropertyRouter, PropertyResponse) {
        return new ResourceFactory(PropertyRouter, PropertyResponse);
    }])
    .service('PropertyBulk', ['Property', 'BulkFactory', function (Property, BulkFactory) {
        return new BulkFactory(Property, {
            actions: {
                delete: null
            }
        });
    }])
;
