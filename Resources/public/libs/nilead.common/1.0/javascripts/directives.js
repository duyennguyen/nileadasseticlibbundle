angular.module('nilead.common').directive('nlBreadcrumb', ['$rootScope', '$state', function ($rootScope, $state) {
        return {
            restrict: 'A',
            templateUrl: 'breadcrumb.html',
            link: function (scope) {
                scope.state = $state;

                $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                    // Use displayName if provided else use default name property
                    var displayName = scope.state.current.displayName || scope.state.current.name;

                    angular.forEach(scope.state.params, function (param, index) {
                        displayName = displayName.replace('{:' + index + '}', param);
                    });

                    scope.displayName = displayName;
                })
            }
        };
    }]).directive('nlServerError', function () {
        return {
            restrict: 'A',
            require: '?ngModel', link: function (scope, element, attrs, ctrl) {
                return element.on('change', function () {
                    return scope.$apply(function () {
                        return ctrl.$setValidity('server', true);
                    });
                });
            }
        };
    }).directive('nlConfirmClick', function () {
        return {
            restrict: 'A',
            link: function (scope, elt, attrs) {
                elt.bind('click', function (e) {
                    var message = attrs.nlConfirmation || "Are you sure you want to do that?";
                    if (window.confirm(message)) {
                        var action = attrs.nlConfirmClick;
                        if (action) {
                            scope.$apply(scope.$eval(action));
                        }
                    }
                });
            }
        };
    })

    // Automatic generate slug based on another model, this model name given in
    // directive value paire.
    //
    // This directive also required "ngModel" to store sluggified string.
    //
    // Usage:
    //     data-nl-slug="form.nilead_blog.name"
    //     data-ng-model="form.nilead_blog.slug"
    .directive('nlSlug', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs) {
                var getter = $parse(attrs.ngModel);
                var setter = getter.assign;

                scope.$watch(attrs.nlSlug, function (input) {
                    var currentSlug = getter(scope);
                    // we only try to update slug if it is not already set
                    if ((undefined === currentSlug || '' == currentSlug) && undefined !== input) {
                        var slug = removeDiacritics(input).replace(/[^0-9A-Za-z]/g, '-').replace(/\-+/g, '-').replace(/^\-+|\-+$/gm, '');

                        setter(scope, slug);
                    }
                })
            }
        }
    }])

    // Automatic initialize element value to model or attr value
    //
    // Usage:
    //    data-nl-initial
    //    data-nl-initial="some value"
    .directive('nlInitial', ['$parse', '$timeout', function ($parse, $timeout) {
        return {
            restrict: 'A',
            priority: 1000,
            link: {
                pre: function (scope, element, attrs) {
                    var getter, setter, value;
                    var model = attrs.nlModel || attrs.ngModel;
                    var lazy = attrs.nlLazy || undefined;
                    var lazyPattern = new RegExp('\\$');

                    if (undefined === model) {
                        console.error('The required attribute "ngModel" or "nlModel" is not defined.');
                    }

                    if (lazyPattern.test(model)) {
                        lazy = true;
                    }

                    // Handle elements which can't have "value" attribute
                    if (- 1 !== _.indexOf(['SELECT', 'TEXTAREA'], element.get(0).tagName)) {
                        value = attrs.nlInitial || element.val() || undefined;
                    } else if (attrs.hasOwnProperty('value')) {
                        // Todo: handle ng-true-value & ng-false-value
                        if (attrs.hasOwnProperty('type') && (
                            'checkbox' === attrs.type.toLowerCase() ||
                            'radio' === attrs.type.toLowerCase()
                            )) {
                            if (element.prop('checked')) {
                                value = attrs.nlInitial || element.val() || true;
                            } else {
                                value = undefined;
                                return;
                            }
                        } else {
                            value = attrs.nlInitial || element.val() || null;
                        }
                    }

                    // Numeric expression, allowed positive, negative and decimal
                    var numeric = new RegExp('^-?(\\d+\\.?\\d*)+$|^(\\d*\\.?\\d+)+$', 'g');
                    if ('string' === typeof value && numeric.test(value)) {
                        value = parseFloat(value);
                    }

                    try {
                        getter = $parse(model);
                    } catch (e) {
                    }

                    if (getter && ! lazy) {
                        setter = getter.assign;
                        setter(scope, value);
                    } else {
                        $timeout(function () {
                            getter = $parse(attrs.ngModel || attrs.nlModel);
                            setter = getter.assign;
                            setter(scope, value);
                        });
                    }
                }
            }
        };
    }]).directive('nlDatepicker', ['$parse', function ($parse) {
        var defaultOptions = {
            pickTime: false //en/disables the time picker
        };
        return {
            restrict: 'A', link: function (scope, element, attrs) {
                var options = {};

                if (attrs.nlDatepicker) {
                    try {
                        eval('options = ' + attrs.nlDatepicker);
                    } catch (e) {
                        options = attrs.nlDatepicker;
                    }
                }

                if ('string' === typeof options && scope[options]) {
                    options = scope[options];
                }

                var getter = $parse(attrs.nlModel);
                var setter = getter.assign;
                var value = getter(scope);

                element.datetimepicker(angular.extend(options, defaultOptions));

                element.on('dp.change', function (event) {
                    var date = event.date;

                    if ('start' == attrs.nlPickMode) {
                        date.hours(0, 0, 1);
                    } else {
                        date.hours(23, 59, 59);
                    }
                    setter(scope, 'timestamp' === attrs.nlType ? date.unix() : date.format('YYYY-MM-DD'));
                });

                element.attr('type', 'text').data('DateTimePicker');

                if (value) {
                    // we only need to convert timestamp
                    var date = new Date('timestamp' === attrs.nlType ? value * 1000 : value);
                    element.data('DateTimePicker').setDate(date);
                }
            }
        };
    }]).directive('nlDatepickerButton', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('click', function () {
                    angular.element(attrs.nlDatepickerButton).focus();
                });
            }
        };
    }).directive('nlFormCollection', ['$compile', '$parse', function (compile, parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var options = {};

                if (attrs.formCollection) {
                    try {
                        eval('options = ' + attrs.formCollection);
                    } catch (e) {
                        options = attrs.formCollection;
                    }
                }

                if ('string' === typeof options && scope[options]) {
                    options = scope[options];
                }

                if (! options.onPreAdd) {
                    options.onPreAdd = function (args) {
                        args.item = compile(args.item)(scope);

                        // Hard code for option value position
                        var position = args.holder.data['index'] || args.holder.children().length;
                        args.item.find('input[name="position"]').val(position).trigger('input');
                    };
                }

                if (! options.onPostDelete) {
                    options.onPostDelete = function (args) {
                        try {
                            var setter, getter = parse(args.element.data('nlModel'));
                            setter = getter.assign;
                            setter(scope, undefined);
                        } catch (e) {
                            console.error(e);
                        }
                    };
                }

                element.formCollection(options);
            }
        }
    }]).directive('nlView', ['NILEAD_EVENTS', 'EventListener', 'ResponseProcessor', '$compile', function (NILEAD_EVENTS, EventListener, ResponseProcessor, $compile) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                // We have to do this because the first message might be broadcasted
                // before the directive is initialized
                if ('' != ResponseProcessor.getView(attrs.nlView)) {
                    element.html(ResponseProcessor.getView(attrs.nlView));
                    $compile(element.contents())(scope);
                }

                EventListener.on(NILEAD_EVENTS.UPDATE_VIEW + attrs.nlView, function () {
                    element.html(ResponseProcessor.getView(attrs.nlView));
                    $compile(element.contents())(scope);
                });
            }
        }
    }]).directive('nlSrefRoot', ['$state', function ($state) {
        return {
            restrict: 'A',
            controller: ['$scope', '$element', function ($scope, $element) {
                var activeClass;
                var submenuClass;

                $scope.maxMatchLevel = 0;

                function isMatch(attrs) {
                    //console.log($state);
                    if (typeof attrs.nlSrefActiveEq !== 'undefined') {
                        return $state.is(attrs.nlSrefState, {});
                    } else if (typeof attrs.nlSrefState !== 'undefined') {
                        var states = attrs.nlSrefState.split(',');
                        for (var index in states) {
                            if ($state.includes(states[index])) {
                                return true;
                            }
                        }

                        return false;
                    }
                }

                this.setActiveClass = function (active) {
                    activeClass = active;
                };

                this.setSubmenuClass = function (submenu) {
                    submenuClass = submenu;
                };

                this.resetMaxMatchLevel = function () {
                    $scope.maxMatchLevel = 0;
                };

                this.setMaxMatchLevel = function (level) {
                    $scope.maxMatchLevel = level > $scope.maxMatchLevel ? level : $scope.maxMatchLevel;
                };

                this.update = function (element, attrs) {
                    if (isMatch(attrs)) {
                        var parents = element.parentsUntil($element);
                        this.setMaxMatchLevel(parents.length);
                        parents.addClass(activeClass);
                        element.addClass(activeClass);
                        if ((element.children()).length > 1 || parents.length > 1) {
                            $element.addClass(submenuClass);
                        } else {
                            $element.removeClass(submenuClass);
                        }
                    }
                }

            }], link: {
                pre: function (scope, element, attrs, Ctrl) {
                    var activeClass = attrs.nlSrefActive || 'active';
                    var submenuClass = attrs.nlSrefSubmenu || 'nl-active-submenu';

                    Ctrl.setActiveClass(activeClass);
                    Ctrl.setSubmenuClass(submenuClass);

                    scope.$on('$stateChangeSuccess', function () {
                        Ctrl.resetMaxMatchLevel();
                        element.find('.' + activeClass).removeClass(activeClass);
                    });
                }
            }
        }
    }]).directive('nlSrefActive', [function () {
        return {
            restrict: 'A',
            require: "^nlSrefRoot", link: function (scope, element, attrs, Ctrl) {
                scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                    Ctrl.update(element, attrs)
                });
            }
        }
    }]).directive('nlValidationValue', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var message = element.text();
                if (message.indexOf('{{ value }}') > -1) {

                    scope.$watch(attrs.nlValidationValue + '.$viewValue', function (newVal, oldVal) {
                        element.text(message.replace('{{ value }}', newVal));
                    })
                }
            }
        }
    }]).directive('nlNestedTreeItem', [function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.css('padding-left', 10 * parseInt(attrs.nlLevel))
            }
        }
    }]).directive('nlTransition', [function () {
        return {
            restrict: 'A',
            scope: {
                nlTransition: '=',
                nlId: '@'
            },
            controller: ['$scope', function ($scope) {
                this.transit = function (transition) {
                    $scope.nlTransition.transit({id: $scope.nlId}, {transition: transition});
                };
            }]
        }
    }]).directive('nlTransit', [function () {
        return {
            restrict: 'A',
            require: '^nlTransition',
            link: function (scope, element, attrs, Ctrl) {
                element.bind('click', function() {
                    Ctrl.transit(attrs.nlTransit);
                });
            }
        }
    }]).directive('nlTooltip', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.tooltip();

                if ('string' === typeof attrs.nlTooltip && attrs.nlTooltip.length > 0) {

                    scope.$watch(attrs.nlTooltip, function (newValue, oldValue) {
                        if (newValue) {
                            element.tooltip('enable');
                        } else {
                            element.tooltip('disable');
                        }
                    });
                }
            }
        }
    }]);
