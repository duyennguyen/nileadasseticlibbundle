<?php
/**
 * Created by Rubikin Team.
 * ========================
 * Date: 2014-10-07
 * Time: 02:22:57 AM
 *
 * This file is a part of Nilead project.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\AsseticLibBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

use Nilead\AsseticLibBundle\DependencyInjection\Compiler\LibPass;

/**
 * Nilead Assetic Lib Bundle
 */
class NileadAsseticLibBundle extends Bundle
{
    /**
     * {@inheritDoc}
     */
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new LibPass());
    }
}
