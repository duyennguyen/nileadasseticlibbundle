angular.module('nilead.common')
    .filter('iif', function () {
        return function (input, trueValue, falseValue) {
            return input ? trueValue : falseValue;
        };
    })
    .filter('nl_image', function () {
        return function (input, filter) {
            return 'string' == typeof input ? input.replace("replaceable_filter", filter) : '';
        };
    })
    .filter('nl_bytes', function() {
        return function(bytes, precision) {
            if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
            if (typeof precision === 'undefined') precision = 1;
            var units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB'],
                number = Math.floor(Math.log(bytes) / Math.log(1024));
            return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
        }
    })
    .filter('nl_price', ['$locale', function ($locale) {
        return function(price, currencyCode) {
            // if there is no currency passed in, lets use the default currency
            if (typeof currencyCode === 'undefined') {
                currencyCode = Currency.getDefaultCurrency();
            }

            currency = Currency.get(currencyCode);

            return accounting.formatMoney((price / currency.divisor), currency.symbol, currency.precision, ".", ",");
        }
    }])
    .filter('nl_trans', function () {
        return function(key, params, domain) {
            if ('object' != typeof params) {
                params = {};
            }

            Translator.trans(key, params, domain);
        }
    })
    .filter('nl_empty', function () {
        return function (obj) {
            if (undefined === obj) {
                return true;
            } else if (obj instanceof Array) {
                return 0 === obj.length;
            } else if (obj instanceof Object) {
                var i;

                for (i in obj) {
                    if (obj.hasOwnProperty(i)) {
                        return false;
                    }
                }

                return true;
            } else {
                console.error('InvalidArgumentException: The "nl-empty" filter expect obj is an array or object, ' + (typeof obj) + ' given.');
            }
        };
    })

    .filter('nl_copy', function () {
        return function (source, dest) {
            _.extend(source, dest);
        };
    })

    .filter('nl_has_element', function () {
        return function (obj) {
            if (undefined === obj) {
                return false;
            } else if (obj instanceof Array) {
                return 0 !== obj.length;
            } else if (obj instanceof Object) {
                var i;

                for (i in obj) {
                    if (obj.hasOwnProperty(i)) {
                        return true;
                    }
                }

                return false;
            } else {
                console.error('InvalidArgumentException: The "nl-empty" filter expect obj is an array or object, ' + (typeof obj) + ' given.');
            }
        };
    })

;
