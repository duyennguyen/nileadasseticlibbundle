angular.module('nilead.common')
    .provider('File', function () {
        this.$get = [function () {
            return {
                change: function (event) {
                    var element = event.target;
                    var $element = angular.element(element);
                    var files = [];

                    if (element.multiple) {
                        angular.forEach(element.files, function(file, index){
                            this.push(file);
                        }, files);
                    } else {
                        files = element.files[0];
                    }

                    return files;
                },
                generateForm: function (model) {
                    var formData = new FormData();

                    var helper = function () {
                        return {
                            generateName: function (prefix, name) {
                                var segments = [];
                                angular.copy(prefix, segments);
                                segments.push(name);
                                name = segments.shift();

                                while (segments.length) {
                                    name += '[' + segments.shift() + ']';
                                }

                                return name;
                            },
                            addFields: function (fields, namePrefix) {
                                angular.forEach(fields, function (field, name) {
                                    if ('string' === typeof name && '$' === name.substr(0, 1)) {
                                        return;
                                    }

                                    if ('array' !== typeof field && 'object' !== typeof field) {
                                        this.append(helper.generateName(namePrefix, name), undefined === field ? '' : field);
                                    } else if (field instanceof File) {
                                        this.append(helper.generateName(namePrefix, name), field);
                                    } else {
                                        var prefix = [];
                                        angular.copy(namePrefix, prefix);
                                        prefix.push(name);

                                        helper.addFields(field, prefix);
                                    }
                                }, formData);
                            }
                        }
                    }();

                    helper.addFields(model, []);

                    return formData;
                }
            }
        }]
    })
;
