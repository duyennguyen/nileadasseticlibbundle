angular.module('nilead.security.permission', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('PermissionRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'security',
            resource: 'permission',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('PermissionResponse', ['ResponseFactory', 'PermissionRouter',
        function (ResponseFactory, PermissionRouter) {
            return new ResponseFactory(PermissionRouter);
        }
    ])
    .service('Permission', ['ResourceFactory', 'PermissionRouter', 'PermissionResponse',
        function (ResourceFactory, PermissionRouter, PermissionResponse) {
            return new ResourceFactory(PermissionRouter, PermissionResponse, function (Resource, Router, Response) {
                return {
                    togglePermission: function (id, permission, attribute, success, error) {
                        var event = this.getEvent('toggle_permission');

                        return Resource.post(Router.generate('toggle_permission', {id: id}), {
                                permission: permission,
                                attribute: attribute
                            },
                            function(response) {
                                return Response.success(response, success, event);
                            },
                            function(response) {
                                return Response.error(response, error, event);
                            })
                            ;
                    }
                }
            });
        }
    ])
    .service('PermissionManager', ['Permission', 'AccountGroup',
        function (Permission, AccountGroup) {
            var self = {
                accountGroup: null,
                accountGroups: [],
                attributes: [],
                permissions: [],
                views: {},
                activeView: {},
                viewMode: 'simple',
                viewModes: {
                    'simpled': 'Simple',
                    'advanced': 'Advanced'
                },
                viewType: 'accountGroups',
                viewTypes: {
                    accountGroups: 'Account Groups',
                    permissions: 'Resources'
                }
            };

            Permission.get('list', {_format: 'json'}, function (response) {
                self.accountGroups = response.data.accountGroups;
                self.permissions = response.data.permissions;

                for (var role in self.permissions) {
                    self.attributes = _.union(self.attributes, self.permissions[role]);
                }

                angular.forEach(self.viewTypes, function (label, viewType) {
                    if (undefined === self.views[viewType]) {
                        self.views[viewType] = [];
                    }

                    angular.forEach(self[viewType], function (subject, index) {
                        var view;

                        if ('permissions' === viewType) {
                            subject = {
                                name: index,
                                attributes: subject
                            };
                        }

                        view = new View(subject, viewType, self);
                        if (undefined === self.activeView[viewType]) {
                            self.activeView[viewType] = view;
                        }

                        this.push(view);
                    }, self.views[viewType]);
                });
            }, false);

            /**
             * Object list view model
             *
             * @param {Object} subject
             * @param {String} type
             * @param {Object} parent
             *
             * @returns {Object}
             */
            var View = function (subject, type, parent) {
                /**
                 * Protected properties & methods on view model
                 *
                 * @type {object}
                 */
                var self = {
                    subject: subject,
                    type: type,

                    getGrantedPermissions: function () {
                        if ('permissions' === this.type) {
                            return parent.activeView['accountGroups'].getSubject().grantedPermissions[this.subject.name];
                        } else if ('accountGroups' === this.type) {
                            return this.subject.grantedPermissions[parent.activeView['permissions'].getName()];
                        }

                        return [];
                    },

                    getInheritedPermission: function (attribute) {
                        if ('permissions' === this.type) {
                            return parent.activeView['accountGroups'].getSubject().reachableGrantedPermissions[this.subject.name];
                        } else if ('accountGroups' === this.type) {
                            return this.subject.reachableGrantedPermissions[parent.activeView['permissions'].getName()];
                        }

                        return [];
                    },

                    getDeniedPermission: function (attribute) {
                        if ('permissions' === this.type) {
                            return parent.activeView['accountGroups'].getSubject().deniedPermissions[this.subject.name];
                        } else if ('accountGroups' === this.type) {
                            return this.subject.deniedPermissions[parent.activeView['permissions'].getName()];
                        }

                        return [];
                    },

                    isParent: function (view) {
                        var activeView;
                        if ('permissions' === this.type) {
                            activeView = parent.activeView['accountGroups'];
                        } else {
                            activeView = this;
                        }

                        console.log(view);
                    }
                };

                /**
                 * Public methods on View model
                 *
                 * @type {object}
                 */
                return {
                    /**
                     * Gets the view type.
                     * The view type must be a part of parent view types list
                     *
                     * @returns {String}
                     */
                    getType: function () {
                        return self.type;
                    },

                    /**
                     * Gets the view subject.
                     * The view subjects are different based on the view type.
                     *
                     *   + permission: The object with permission name & list of attributes
                     *   + account_group: The account group object
                     *
                     * @returns {Object}
                     */
                    getSubject: function () {
                        return self.subject;
                    },

                    /**
                     * Gets the view display name. Based on subject name property
                     *
                     * @returns {String}
                     */
                    getName: function () {
                        return self.subject.name;
                    },

                    /**
                     * Checks whether is this view currently active.
                     *
                     * @returns {boolean}
                     */
                    isActive: function () {
                        return _.isEqual(parent.activeView[parent.viewType], this);
                    },

                    /**
                     * Checks whether this view has the given attribute
                     *
                     * @param {String} attribute
                     *
                     * @returns {boolean}
                     */
                    hasAttribute: function (attribute) {
                        if ('permissions' === self.type) {
                            return -1 !== self.subject.attributes.indexOf(attribute);
                        } else if ('accountGroups' === self.type) {
                            return -1 !== parent.activeView['permissions'].getSubject().attributes.indexOf(attribute);
                        }

                        return false;
                    },

                    /**
                     * Checks whether the active account group has granted permission with specific attribute
                     *
                     * @param {String} attribute
                     *
                     * @returns {boolean}
                     */
                    isGrantedPermission: function (attribute) {
                        return _.contains(self.getGrantedPermissions(), attribute);
                    },

                    /**
                     * Checks whether is the active group has inherited permission with the given attribute
                     *
                     * @param {string} attribute
                     *
                     * @returns {boolean}
                     */
                    isInheritedPermission: function (attribute) {
                        return _.contains(self.getInheritedPermission(), attribute);
                    },

                    /**
                     * Checks whether is the active group has denied permission with the given attribute
                     *
                     * @param {string} attribute
                     *
                     * @returns {boolean}
                     */
                    isDeniedPermission: function (attribute) {
                        return _.contains(self.getDeniedPermission(), attribute);
                    },

                    togglePermission: function (attribute) {
                        var permission, accountGroup;
                        var _self = this;

                        if ('permissions' === this.getType()) {
                            permission = this.getName();
                            accountGroup = parent.activeView['accountGroups'].getSubject();
                        } else {
                            permission = parent.activeView['permissions'].getName();
                            accountGroup = this.getSubject();
                        }

                        Permission.togglePermission(accountGroup.id, permission, attribute, function (response) {
                            accountGroup.grantedPermissions = response.data.data.account_group.grantedPermissions;
                            accountGroup.deniedPermissions = response.data.data.account_group.deniedPermissions;

                            _self.updateViews();
                        });
                    },

                    updateViews: function () {
                        angular.forEach(parent.views['accountGroups'], function (view) {
                            console.log(self.isParent(view));
                        });
                    }
                }
            };

            /**
             * Public methods on PermissionManager service
             *
             * @type {object}
             */
            return {
                getViewMode: function () {
                    return self.viewMode;
                },

                setViewMode: function (viewMode) {
                    self.viewMode = viewMode;

                    return this;
                },

                isAdvancedMode: function () {
                    return 'advanced' === this.getViewMode();
                },

                getViewModes: function () {
                    return self.viewModes;
                },

                getViewType: function () {
                    return self.viewType;
                },

                setViewType: function (viewType) {
                    self.viewType = viewType;

                    return this;
                },

                getViewTypes: function () {
                    return self.viewTypes;
                },

                getViews: function () {
                    return self.views[self.viewType];
                },

                getActiveResource: function () {
                    return self.activeView['accountGroups'];
                },

                getActiveView: function () {
                    return self.activeView[self.viewType];
                },

                setActiveView: function (view) {
                    self.activeView[self.viewType] = view;
                },

                getAttributes: function () {
                    return self.attributes;
                },

                getListView: function () {
                    return 'accountGroups' === self.viewType ? self.views.permissions : self.views.accountGroups;
                }
            }
        }
    ])
    //.service('PermissionManager_', ['Permission', 'AccountGroup', function (Permission, AccountGroup) {
    //    var viewTypes = {
    //        group: 'Groups'
    //    };
    //
    //    var viewType = 'group';
    //    var view;
    //    var attributes = [], groups, permissions;
    //
    //    Permission.get('list', {_format: 'json'}, function (response) {
    //        groups = response.data.groups;
    //        permissions = response.data.permissions;
    //
    //        for (var role in permissions) {
    //            attributes = _.union(attributes, permissions[role]);
    //        }
    //    }, false);
    //
    //    return {
    //        getViewTypes: function () {
    //            return viewTypes;
    //        },
    //
    //        getViewType: function () {
    //            return viewType;
    //        },
    //
    //        setViewType: function (_viewType) {
    //            viewType = _viewType;
    //
    //            return this;
    //        },
    //
    //        getViews: function () {
    //            return 'group' === this.getViewType() ? groups : permissions;
    //        },
    //
    //        hasAttribute: function (resource, attr) {
    //            return -1 !== permissions[resource].indexOf(attr);
    //        },
    //
    //        getAttributes: function () {
    //            return attributes;
    //        },
    //
    //        hasPermission: function (resource, attr) {
    //            var view = this.getView();
    //
    //            if ( ! view || ! view.permissions[resource]) {
    //                return false;
    //            }
    //
    //            return -1 !== view.permissions[resource].indexOf(attr);
    //        },
    //
    //        addPermission: function (resource, attr) {
    //            var view = this.getView();
    //
    //            if (view.permissions[resource]) {
    //                view.permissions[resource].push(attr);
    //            } else {
    //                view.permissions[resource] = [attr];
    //            }
    //        },
    //
    //        removePermission: function (resource, attr) {
    //            var view = this.getView();
    //
    //            view.permissions[resource].splice(view.permissions[resource].indexOf(attr), 1);
    //        },
    //
    //        getPermissions: function () {
    //            return permissions;
    //        },
    //
    //        getView: function () {
    //            var activeView = this.getViews();
    //
    //            if (undefined === activeView) {
    //                return [];
    //            }
    //
    //            activeView = activeView.filter(function (element) {
    //                return view === element.id;
    //            });
    //
    //            return activeView[0];
    //        },
    //
    //        setView: function (_view) {
    //            view = _view;
    //
    //            return this;
    //        },
    //
    //        togglePermission: function (resource, attr) {
    //            var state = ! this.hasPermission(resource, attr);
    //            var view = this.getView();
    //            var manager = this;
    //
    //            Permission.togglePermission(view.id, resource, attr, function (response) {
    //                if (state) {
    //                    manager.addPermission(resource, attr);
    //                } else {
    //                    manager.removePermission(resource, attr);
    //                }
    //            });
    //        }
    //    }
    //}])
;