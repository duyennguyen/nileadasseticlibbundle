angular.module('nilead.common')
    .directive('nlFile', ['File', '$parse', '$rootScope', function(File, $parse, $rootScope) {
        return {
            restrict: 'A',
            link: function (scope, element, attributes) {
                $rootScope.$broadcast('NILEAD_FILE_ADDED', element, scope.$index);

                element.bind('change', function (event) {
                    var file, getter, setter;

                    var file = File.change(event);
                    var getter = $parse(attributes.nlModel);
                    var setter = getter.assign;

                    setter(scope, file);
                    scope.$apply();

                    $rootScope.$broadcast('NILEAD_FILE_UPLOAD_BROWSED', element, scope.$index, file);
                });
            }
        };
    }])
    .directive('nlBrowse', function() {
        return {
            restrict: 'A',
            scope: {
                holder: '@',
                target: '@'
            },
            link: function (scope, element, attributes) {
                element.bind('click', function (event) {
                    if (scope.target) {
                        angular.element(scope.target).trigger('click');
                    } else {
                        element.closest(scope.holder).find('[type="file"]').trigger('click');
                    }
                });
            }
        };
    })
    .directive('nlThumb', function () {
        return {
            restrict: 'A',
            // template: '<img />',
            link: function (scope, element, attributes) {
                scope.$watch(attributes.nlThumb, function (file) {
                    if (file instanceof File) {
                        element.html('<img />');

                        var img = element.find('img');
                        var reader = new FileReader();

                        // Closure to capture the file information.
                        reader.onload = function(event) {
                            img.attr('src', event.target.result);
                        };

                        // Read in the image file as a data URL.
                        reader.readAsDataURL(file);
                    }
                });
            }
        }
    })
;
