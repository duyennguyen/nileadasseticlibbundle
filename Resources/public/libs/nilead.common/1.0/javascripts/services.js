angular.module('nilead.common')
    .constant('NILEAD_EVENTS', nl.events)
    .service('EventListener', function () {
        return nl.eventListener;
});
