angular.module('nilead.guide.guide', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })

    .service('GuideRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'guide',
            resource: 'guide',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('GuideResponse', ['ResponseFactory', 'GuideRouter', function (ResponseFactory, GuideRouter) {
        return new ResponseFactory(GuideRouter);
    }])
    .service('Guide', ['ResourceFactory', 'GuideRouter', 'GuideResponse', function (ResourceFactory, GuideRouter, GuideResponse) {
        return new ResourceFactory(GuideRouter, GuideResponse);
    }])
    .service('GuideBulk', ['Guide', 'BulkFactory', function (Guide, BulkFactory) {
        return new BulkFactory(Guide, {
            actions: {
                active: null
            }
        });
    }])
;
