angular.module('nilead.inventory.supplier', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('SupplierRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'inventory',
            resource: 'supplier',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('SupplierResponse', ['ResponseFactory', 'SupplierRouter', function (ResponseFactory, SupplierRouter) {
        return new ResponseFactory(SupplierRouter);
    }])
    .service('Supplier', ['ResourceFactory', 'SupplierRouter', 'SupplierResponse', function (ResourceFactory, SupplierRouter, SupplierResponse) {
        return new ResourceFactory(SupplierRouter, SupplierResponse);
    }])
    .service('SupplierBulk', ['Supplier', 'BulkFactory', function (Supplier, BulkFactory) {
        return new BulkFactory(Supplier, {
            actions: {
                delete: null
            }
        });
    }])
;
