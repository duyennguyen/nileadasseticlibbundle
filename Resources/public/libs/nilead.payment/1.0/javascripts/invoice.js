angular.module('nilead.payment.invoice', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('InvoiceRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'payment',
            resource: 'invoice',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('InvoiceResponse', ['ResponseFactory', 'InvoiceRouter', function (ResponseFactory, InvoiceRouter) {
        return new ResponseFactory(InvoiceRouter);
    }])
    .service('Invoice', ['ResourceFactory', 'InvoiceRouter', 'InvoiceResponse', 'Resource', function (ResourceFactory, InvoiceRouter, InvoiceResponse, Resource) {
        return new ResourceFactory(InvoiceRouter, InvoiceResponse, function(Resource, InvoiceRouter, InvoiceResponse){
            return {
                updateSummary: function (id, summary, success, error) {
                    var event = this.getEvent('update_summary');
                    return Resource.post(InvoiceRouter.generate('update_summary', {id: id, _format:'json'}), summary,
                        function (response) {
                            return InvoiceResponse.success(response, success, event);
                        },
                        function (response) {
                            return InvoiceResponse.error(response, error, event);
                    });
                },
                create: function (params, data, success, error) {
                    var event = this.getEvent('create');
                    return Resource.post(InvoiceRouter.generate('create', params), data,
                        function(response) {
                            return InvoiceResponse.success(response, success, event);
                        },
                        function(response) {
                            return InvoiceResponse.error(response, error, event);
                        }
                    );
                }
            }
        });
    }])
    .service('InvoiceBulk', ['Invoice', 'BulkFactory', function (Invoice, BulkFactory) {
        return new BulkFactory(Invoice, {
            actions: {
                active: null
            }
        });
    }])
    .service('InvoiceItemHandler', ['$rootScope', 'Invoice', function ($rootScope, Invoice) {
        var InvoiceItemHandler = function (Invoice) {

            /**
             * @type {Array}
             */
            var items = [];

            /**
             * @type {Array}
             */
            var itemIds = [];

            /**
             * @type {Array}
             */
            var selectedItems = [];

            /**
             * @type {Array}
             */
            var selectedIds = [];

            /**
             * @type {Array}
             */
            var instances = [];

            return ({
                createInstance: function (name) {
                    if (undefined !== name) {
                        name = Invoice.getRouter().getResource() + '.' + name;

                        if (undefined === instances[name]) {
                            instances[name] = new InvoiceItemHandler(Invoice);
                        } else {
                            instances[name].reset();
                        }

                        return instances[name];
                    } else {
                        return new InvoiceItemHandler(Invoice);
                    }
                },
                reset: function () {
                    items = [];
                    itemIds = [];
                    selectedItems = [];
                    selectedIds = [];
                },
                addItem: function (item, selected) {
                    if (_.indexOf(itemIds, item.id) == -1){
                        items.push(item);
                        itemIds.push(item.id);
                        if (selected) {
                            selectedItems.push(item);
                            selectedIds.push(item.id);
                        }
                    }
                },
                getSelectedItems: function () {
                    return selectedItems;
                },
                getItems: function () {
                    return items;
                },
                isSelected: function (id) {
                    return -1 !== _.indexOf(selectedIds, id);
                },
                toggleItem: function (id) {
                    var index = _.indexOf(selectedIds, id);
                    if (-1 === index) {
                        selectedItems.push(items[_.indexOf(itemIds, id)]);
                        selectedIds.push(id);
                    } else {
                        selectedItems.splice(index, 1);
                        selectedIds.splice(index, 1);
                    }
                }
            });
        };

        return new InvoiceItemHandler(Invoice);
    }])
;
