angular.module('nilead.common')
    .service('nlHistory', ['$parse', function ($parse) {
        return {
            decorate: function (object, scope) {
                var setFunc,
                    persistCallbacks = [],
                    revertCallbacks = [],
                    history = [],
                    processing = false,
                    objectType = typeof object;

                if ('string' === objectType && 'object' === typeof scope) {
                    var getter = setter = $parse(object);
                    var setter = getter.assign;
                    setFunc = function (val) {
                        processing = true;
                        setter(scope, val);
                    }

                } else if ('object' === objectType) {
                    setFunc = function (val) {
                        object = val;
                    }
                } else {
                    return;
                }

                var decorated = {
                    persist: function (data, title) {
                        var count = this.count();
                        if (count == 0 || !angular.equals(data, history[count - 1])) {
                            history.push({data: angular.copy(data), title: title});

                            var revision = history.length - 1;
                            angular.forEach(persistCallbacks, function(callback, key) {
                                callback(revision, data);
                            });

                            return revision;
                        }
                    },
                    addPersistCallback: function (callback) {
                        if ('function' === typeof callback) {
                            persistCallbacks.push(callback);
                        }
                    },
                    revert: function (revision) {
                        // if no revision is passed, lets try to revert to the last one
                        if ('undefined' === typeof revision) {
                            revision = history.length - 2;
                        }

                        if ('undefined' !== typeof history[revision]) {
                            setFunc(angular.copy(history[revision].data));

                            // remove
                            history.splice(revision + 1, history.length - (revision + 1));

                            angular.forEach(revertCallbacks, function(callback, key) {
                                callback(revision, history[revision].data);
                            });

                            return true;
                        }

                        return false;
                    },
                    addRevertCallback: function (callback) {
                        if ('function' === typeof callback) {
                            revertCallbacks.push(callback);
                        }
                    },
                    list: function () {
                        return history;
                    },
                    count: function () {
                        return history.length;
                    },
                    reset: function () {
                        history = [];
                    }
                };

                // auto watch if applicable
                if ('string' === objectType) {
                    var persist = _.debounce(function (newVal, oldVal) {
                        if (! processing) {
                            decorated.persist(newVal);
                        }

                        processing = false;
                    }, 350, true);

                    scope.$watch(object, function (newVal, oldVal) {
                        persist(newVal, oldVal);
                    }, true);
                }

                return decorated;
            }
        }
    }]);
