angular.module('nilead.media.gallery', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('GalleryRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'media',
            resource: 'gallery',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('GalleryResponse', ['ResponseFactory', 'ImageAwareResponseFactory', 'GalleryRouter', function (ResponseFactory, ImageAwareResponseFactory, GalleryRouter) {
        return ImageAwareResponseFactory(new ResponseFactory(GalleryRouter), function (response, resource) {
            var path = ['data', 'data', resource, 'items'], images = {};

            if (_.hasPath(response, path)) {
                angular.forEach(_.getPath(response, path), function (galleryItem) {
                    angular.forEach(galleryItem.images, function (image) {
                        images[image.id] = image;
                    });
                });
            }

            return images;
        });
    }])

    .service('Gallery', ['ResourceFactory', 'GalleryRouter', 'GalleryResponse', function (ResourceFactory, GalleryRouter, GalleryResponse) {
        return new ResourceFactory(GalleryRouter, GalleryResponse);
    }])
    .service('GalleryBulk', ['Gallery', 'BulkFactory', function (Gallery, BulkFactory) {
        return new BulkFactory(Gallery, {
            actions: {
                enable: null,
                disable: null,
                delete: null
            }
        });
    }])
    .service('GallerySearch', ['GalleryRouter', 'SearchFactory', function (GalleryRouter, SearchFactory) {
        return function (id, preset, action) {
            if ( ! angular.isDefined(action)) {
                action = 'index';
            }

            return SearchFactory(id, {
                action: action,
                router: GalleryRouter,
                entities: [GalleryRouter.getResource()]
            }, 'gallery', preset);
        }
    }])
;
