/**
 * Variable post affect method post in javascript , replace to blog
 */
angular.module('nilead.module.search').service('SearchRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'search',
            resource: 'global',
            params: {
                _format: 'partial'
            }
        });
    }]).factory('SearchFactory', ['Resource', 'Environment', '$q', '$cacheFactory', '$timeout', 'NILEAD_EVENTS', 'EventListener', function (Resource, Environment, $q, $cacheFactory, $timeout, NILEAD_EVENTS, EventListener) {
        var $searches = {};

        return function (id, options, type, preset) {

            if (typeof id === 'undefined') {
                id = 'default';
            }

            if (typeof preset === 'undefined') {
                preset = 'default';
            }

            // same id, different preset should return different object
            id = id + '_' + type + '_' + preset;

            var cache, cacheId = 'nlSearchCache.' + id;

            if (! (cache = $cacheFactory.get(cacheId))) {
                cache = $cacheFactory(cacheId);
            }

            if (! $searches.hasOwnProperty(id)) {
                if ('object' !== typeof options) {
                    options = {};
                }

                // hook to certain events to reset cache
                if (options.hasOwnProperty('entities') && _.isArray(options.entities)) {
                    var resetCache = function () {
                        cache.removeAll();
                    };

                    angular.forEach(options.entities, function (v, k) {
                        EventListener.on(NILEAD_EVENTS.RESOURCE_RESPONSE_SUCCESS.replace('%event%', v + '_create').toUpperCase(), resetCache);
                        EventListener.on(NILEAD_EVENTS.RESOURCE_RESPONSE_SUCCESS.replace('%event%', v + '_update').toUpperCase(), resetCache);
                        EventListener.on(NILEAD_EVENTS.RESOURCE_RESPONSE_SUCCESS.replace('%event%', v + '_delete').toUpperCase(), resetCache);
                    });
                }

                var searching = false;

                $searches[id] = new function (id, options) {
                    var helper = {};

                    // allow helper to be extended
                    if (options.hasOwnProperty('helper')) {
                        helper = options.helper;
                    }

                    helper = angular.extend({
                        'default': function (element, agg, value) {
                            if (agg['aggs']['items'].hasOwnProperty(value.split('_').pop())) {
                                element.parent().removeClass('disabled');
                                element.prop('disabled', false);
                            } else {
                                element.parent().addClass('disabled');
                                element.prop('disabled', true);
                            }
                        }
                    }, helper);

                    var Searcher = {
                        $includeFullAgg: 1,
                        $data: {
                            'queries': {}, 'filters': {}, 'sorters': {}
                        },
                        $currentAggs: '',
                        $params: {},
                        $lastParams: '',
                        $filterView: '',
                        getId: function () {
                            return id;
                        },
                        getParams: function (params) {

                            if ('object' !== typeof params) {
                                params = this.$params;
                            } else {
                                // extends params
                                angular.extend(params, this.$params);
                            }

                            // sanitize
                            for (var i in params) {
                                if (params[i] === null || params[i] === undefined) {
                                    // test[i] === undefined is probably not very useful here
                                    delete params[i];
                                }
                            }

                            var externalParams = params;

                            if ('string' == externalParams.q) {
                                externalParams.q = JSON.parse(externalParams.q);
                            }

                            var internalParams = {
                                type: type,
                                preset: preset,
                                include_full_agg: options.hasOwnProperty('includeFullAgg') ? options.includeFullAgg : this.$includeFullAgg,
                                q: {
                                    queries: this.$data.queries,
                                    filters: this.$data.filters,
                                    sorters: this.$data.sorters,
                                    aggs: this.$data.aggs
                                }
                            };

                            // We must use jQuery here to be able to extend with deep option
                            var extendedParams = jQuery.extend(true, externalParams, internalParams);

                            if ('object' == typeof extendedParams.q) {
                                extendedParams.q = JSON.stringify(extendedParams.q);
                            }
                            return extendedParams;
                        },
                        setData: function (value, path) {
                            this.$data = _.setPath(this.$data, value, path.split('.'), {});
                        },
                        generateRoute: function (params) {
                            return options.router.generate(options.action, params);
                            //return Routing.generate(options['route'], this.getParams(params));
                        },
                        search: function (url, reload, success, error) {

                            if ('string' != typeof url) {
                                // remember the last url params
                                this.$lastParams = url;
                                url = this.generateRoute(this.getParams(url));
                            }

                            if (typeof reload === 'undefined') {
                                reload = false;
                            }

                            EventListener.emit(NILEAD_EVENTS.SEARCH_START + this.getId());

                            var $this = this;

                            if (searching) {
                                return $timeout(function() {
                                    return $this.search(url, reload, success, error);
                                }, 100);
                            }

                            if (! reload && angular.isDefined(cache.get(url))) {
                                return $this._success(cache.get(url), success);
                            } else {
                                searching = true;
                                return Resource.get(url,
                                    function (response) {
                                        $this.$includeFullAgg = 0;

                                        // we only cache in success response
                                        cache.put(url, response);
                                        searching = false;
                                        return $this._success(response, success);
                                    },
                                    function (response) {
                                        EventListener.emit(NILEAD_EVENTS.SEARCH_ERROR + $this.getId(), response);
                                        EventListener.emit(NILEAD_EVENTS.SEARCH_END + $this.getId(), response);
                                        searching = false;
                                        return 'function' === typeof error ? error(response) : response;
                                    }
                                );
                            }

                        },
                        reset: function() {
                            this.$data = {
                                'queries': {}, 'filters': {}, 'sorters': {}
                            }
                        },
                        toggleStatus: function (element, key, value, type) {

                            if (this.$currentAggs.hasOwnProperty(key)) {
                                if (helper.hasOwnProperty(type)) {
                                    helper[type](element, this.$currentAggs[key], value);
                                } else {
                                    helper['default'](element, this.$currentAggs[key], value);
                                }
                            } else {
                                element.prop('disabled', false);
                            }
                        },
                        hasCache: function () {
                            return cache.info().size > 0;
                        },
                        resetCache: function () {
                            cache.removeAll();
                        },
                        _success: function (response, success) {
                            EventListener.emit(NILEAD_EVENTS.SEARCH_SUCCESS + id, response);
                            EventListener.emit(NILEAD_EVENTS.SEARCH_END + id, response);

                            if (_.hasPath(response.data.data, ['views', 'filter'])) {
                                this.$filterView = response.data.data.views.filter;

                                // dispatch event
                                EventListener.emit(NILEAD_EVENTS.SEARCH_UPDATE_FILTER_TEMPLATE + this.getId());
                            }

                            if (_.hasPath(response.data.data, ['aggs', 'current'])) {
                                this.$currentAggs = response.data.data.aggs.current;

                                // dispatch event
                                EventListener.emit(NILEAD_EVENTS.SEARCH_UPDATE_FILTER_CURRENT + this.getId());
                            }

                            return 'function' === typeof success ? success(response) : response;
                        }
                    };

                    return Object.create(Searcher);

                }(id, options);

            }

            return $searches[id];
        }
    }]).service('GlobalSearch', ['SearchFactory', 'SearchRouter', function (SearchFactory, SearchRouter) {
        return function (id, preset) {

            return SearchFactory(id, {
                includeFullAgg: 0,
                router: SearchRouter,
                action: 'search',
                entities: ['blog', 'page', 'product', 'order', 'account']
            }, 'global', preset);
        }
    }]);

