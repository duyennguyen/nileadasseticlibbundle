angular.module('nilead.checkout.checkout')
    .service('CheckoutRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'checkout',
            resource: 'checkout',
            params: {
                _format: 'partial'
            }
        });
    }])

    .service('CheckoutResponse', ['ResponseFactory', 'CheckoutRouter', function (ResponseFactory, CheckoutRouter) {
        return new ResponseFactory(CheckoutRouter);
    }])
    .service('CheckoutCommon', function () {
        return function (Resource, CheckoutRouter, CheckoutResponse) {
            return {
                getSplash: function (success, error) {
                    var event = this.getEvent('splash');
                    return Resource.get(CheckoutRouter.generate('splash'), function (response) {
                        return CheckoutResponse.success(response, success, event);
                    }, function (response) {
                        return CheckoutResponse.error(response, error, event);
                    });
                },
                getLoginRegister: function (success, error) {
                    var event = this.getEvent('login_register');
                    return Resource.get(CheckoutRouter.generate('login_register'), function (response) {
                            return CheckoutResponse.success(response, success, event);
                        }, function (response) {
                            return CheckoutResponse.error(response, error, event);
                        });
                },  getInitializeGuestOrder: function (success, error) {
                    var event = this.getEvent('get_initialize_guest_order');
                    return Resource.get(CheckoutRouter.generate('initialize_guest_order'), function (response) {
                        return CheckoutResponse.success(response, success, event);
                    }, function (response) {
                        return CheckoutResponse.error(response, error, event);
                    });
                }, postInitializeGuestOrder: function (postParams, success, error) {
                    var event = this.getEvent('post_initialize_guest_order');
                    return Resource.post(CheckoutRouter.generate('initialize_guest_order'), postParams, function (response) {
                        return CheckoutResponse.success(response, success, event);
                    }, function (response) {
                        return CheckoutResponse.error(response, error, event);
                    });
                }, getInitializeOrder: function (success, error) {
                    var event = this.getEvent('get_initialize_order');
                    return Resource.get(CheckoutRouter.generate('initialize_order'), function (response) {
                            return CheckoutResponse.success(response, success, event);
                        }, function (response) {
                            return CheckoutResponse.error(response, error, event);
                        });
                }, postInitializeOrder: function (postParams, success, error) {
                    var event = this.getEvent('post_initialize_order');
                    return Resource.post(CheckoutRouter.generate('initialize_order'), postParams, function (response) {
                            return CheckoutResponse.success(response, success, event);
                        }, function (response) {
                            return CheckoutResponse.error(response, error, event);
                        });
                }, getUpdateCart: function (clientKey, authToken, success, error) {
                    var event = this.getEvent('get_update_order');
                    return Resource.get(CheckoutRouter.generate('update_cart', {client_key: clientKey, auth_token: authToken}), function (response) {
                            return CheckoutResponse.success(response, success, event);
                        }, function (response) {
                            return CheckoutResponse.error(response, error, event);
                        });
                }, postUpdateCart: function (clientKey, authToken, postParams, success, error) {
                    var event = this.getEvent('post_update_order');
                    return Resource.post(CheckoutRouter.generate('update_cart', {client_key: clientKey, auth_token: authToken}), postParams, function (response) {
                            return CheckoutResponse.success(response, success, event);
                        }, function (response) {
                            return CheckoutResponse.error(response, error, event);
                        });
                }, getUpdateAddresses: function (clientKey, authToken, success, error) {
                    var event = this.getEvent('get_update_address');
                    return Resource.get(CheckoutRouter.generate('update_addresses', {client_key: clientKey, auth_token: authToken}), function (response) {
                            return CheckoutResponse.success(response, success, event);
                        }, function (response) {
                            return CheckoutResponse.error(response, error, event);
                        });
                }, postUpdateAddresses: function (clientKey, authToken, form, success, error) {
                    var event = this.getEvent('post_update_address');
                    return Resource.post(CheckoutRouter.generate('update_addresses', {client_key: clientKey, auth_token: authToken}), form, function (response) {
                            return CheckoutResponse.success(response, success, event);
                        }, function (response) {
                            return CheckoutResponse.error(response, error, event);
                        });
                }, getUpdateExtras: function (clientKey, authToken, success, error) {
                    var event = this.getEvent('get_update_extras');
                    return Resource.get(CheckoutRouter.generate('update_extras', {client_key: clientKey, auth_token: authToken}), function (response) {
                            return CheckoutResponse.success(response, success, event);
                        }, function (response) {
                            return CheckoutResponse.error(response, error, event);
                        });
                }, postUpdateExtras: function (clientKey, authToken, form, success, error) {
                    var event = this.getEvent('post_update_extras');
                    return Resource.post(CheckoutRouter.generate('update_extras', {client_key: clientKey, auth_token: authToken}), form, function (response) {
                            return CheckoutResponse.success(response, success, event);
                        }, function (response) {
                            return CheckoutResponse.error(response, error, event);
                        });
                }, getCheckoutSuccess: function (clientKey, authToken, success, error) {
                    var event = this.getEvent('checkout_success');
                    return Resource.get(CheckoutRouter.generate('checkout_success', {client_key: clientKey, auth_token: authToken}), function (response) {
                            return CheckoutResponse.success(response, success, event);
                        }, function (response) {
                            return CheckoutResponse.error(response, error, event);
                        });
                }, addDiscountCode: function (clientKey, authToken, code, success, error) {
                    var event = this.getEvent('add_discount_code');
                    return Resource.post(CheckoutRouter.generate('add_discount_code', {client_key: clientKey, auth_token: authToken}), {code: code}, function (response) {
                            return CheckoutResponse.success(response, success, event);
                        }, function (response) {
                            return CheckoutResponse.error(response, error, event);
                        });
                }, removeOrderAdjustment: function (clientKey, authToken, id, success, error) {
                    var event = this.getEvent('remove_adjustment');
                    return Resource.post(CheckoutRouter.generate('remove_adjustment', {client_key: clientKey, auth_token: authToken}), {id: id}, function (response) {
                        return CheckoutResponse.success(response, success, event);
                    }, function (response) {
                        return CheckoutResponse.error(response, error, event);
                    });
                }
            }
        }
    })
    .service('Checkout', ['ResourceFactory', 'CheckoutRouter', 'CheckoutResponse', 'CheckoutCommon', function (ResourceFactory, CheckoutRouter, CheckoutResponse, CheckoutCommon) {
        return new ResourceFactory(CheckoutRouter, CheckoutResponse, CheckoutCommon);
    }])

    .factory('backendCheckout', function () {
        var items = {};
        var total = 0;

        return {
            init: function (order) {

            }, removeItem: function (item) {
                item.quantity = 0;
                this.updateItem(item);
            }, addItem: function (id, description, price) {
                if (items.hasOwnProperty(id)) {
                    items[id].quantity += 1;
                } else {
                    items[id] = {id: id, description: description, quantity: 1, unitPrice: parseInt(price)};
                }

                this.updateItem(items[id]);
            }, updateItem: function (item) {
                if (0 == item.quantity) {
                    delete items[item.id];
                } else {
                    item.total = item.quantity * item.unitPrice;
                }

                this.updateCart();
            }, getItems: function () {
                return items;
            }, getTotal: function () {
                return total;
            }, updateCart: function () {
                total = 0;
                angular.forEach(items, function (item, key) {
                    total += item.unitPrice * item.quantity;
                });
            }
        };
    })
    .directive('nlPaymentMethodSelect', ['$compile', function ($compile) {
        return {
            restrict: 'A',
            link: function ($scope, element, attrs) {
                element.bind('click', function () {
                    var holder = angular.element('#nilead_card_' + element.val());
                    var container = angular.element(attrs.container);
                    var others = angular.element('.nilead_container');
                    angular.forEach(others, function (other) {
                        angular.element(other).html('');
                    });
                    container.html(holder.data('prototype'));
                    $compile(container.contents())($scope);
                });
            }
        }
    }]);
