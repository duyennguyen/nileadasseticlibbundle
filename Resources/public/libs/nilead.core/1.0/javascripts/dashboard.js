angular.module('nilead.core.dashboard', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .provider('Dashboard', function () {
        this.$get = ['ResponseProcessor', 'Resource', function (ResponseProcessor, Resource) {
            return {
                getReportData: function () {
                    return Resource.get(Routing.generate('nilead.core.backend.dashboard.get_report_data')).then(function (response) {
                        return response.data;
                    });
                }
            }
        }]
    })
;
