angular.module('nilead.theme.theme_file', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('ThemeFileRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'theme',
            resource: 'theme_file',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('ThemeFileResponse', ['ResponseFactory', 'ThemeFileRouter', function (ResponseFactory, ThemeFileRouter) {
        return new ResponseFactory(ThemeFileRouter);
    }])
    .service('ThemeFile', ['ResourceFactory', 'EventListener', 'NILEAD_EVENTS', 'ThemeFileRouter', 'ThemeFileResponse', function (ResourceFactory, EventListener, NILEAD_EVENTS, ThemeFileRouter, ThemeFileResponse) {
        return new ResourceFactory(ThemeFileRouter, ThemeFileResponse, function (Resource, ThemeFileRouter, ThemeFileResponse) {
            return {
                show: function (themeId, path, data, success, error) {
                    var event = this.getEvent('get_update');
                    return Resource.get(ThemeFileRouter.generate('update', {themeId: themeId, path: path}),
                        function(response) {
                            return ThemeFileResponse.success(response, success, event);
                        },
                        function (response) {

                            return ThemeFileResponse.error(response, error, event);
                        }
                    );
                },
                update: function (themeId, path, data, success, error) {
                    var event = this.getEvent('post_update');
                    return Resource.post(ThemeFileRouter.generate('update', {themeId: themeId, path: path}), data,
                        function(response) {
                            return ThemeFileResponse.success(response, success, event);
                        },
                        function (response) {
                            return ThemeFileResponse.error(response, error, event);
                        }
                    );
                }
            }
        });
    }])
;