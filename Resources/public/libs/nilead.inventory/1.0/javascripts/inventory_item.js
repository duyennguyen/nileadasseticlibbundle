angular.module('nilead.inventory.inventory_item', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('InventoryItemRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'inventory',
            resource: 'inventory_item',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('InventoryItemResponse', ['ResponseFactory', 'InventoryItemRouter', function (ResponseFactory, InventoryItemRouter) {
        return new ResponseFactory(InventoryItemRouter);
    }])
    .service('InventoryItem', ['ResourceFactory', 'InventoryItemRouter', 'InventoryItemResponse', function (ResourceFactory, InventoryItemRouter, InventoryItemResponse) {
        return new ResourceFactory(InventoryItemRouter, InventoryItemResponse);
    }])
    .service('InventoryItemBulk', ['InventoryItem', 'BulkFactory', function (InventoryItem, BulkFactory) {
        return new BulkFactory(InventoryItem, {
            actions: {
                delete: null
            }
        });
    }])
;
