angular.module('nilead.documentor.document', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('DocumentRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'documentor',
            resource: 'document',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('DocumentResponse', ['ResponseFactory', 'DocumentRouter', function (ResponseFactory, DocumentRouter) {
        return new ResponseFactory(DocumentRouter);
    }])
    .service('Document', ['ResourceFactory', 'DocumentRouter', 'DocumentResponse', function (ResourceFactory, DocumentRouter, DocumentResponse) {
        return new ResourceFactory(DocumentRouter, DocumentResponse);
    }])

    .service('DocumentSearch', ['DocumentRouter', 'SearchFactory', function (DocumentRouter, SearchFactory) {
        return function (id, preset, action) {
            if ( ! angular.isDefined(action)) {
                action = 'index';
            }

            return SearchFactory(id, {
                action: action,
                router: DocumentRouter,
                entities: [DocumentRouter.getResource()]
            }, 'document', preset);
        }
    }])
;
