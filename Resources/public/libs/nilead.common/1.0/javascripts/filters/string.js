angular.module('nilead.common')
    .filter('pluralize', function () {
        /**
         * Renders a singular English language noun into its plural form
         * normal results can be overridden by passing in an alternative
         *
         * @param {String} input
         *
         * @return {String}
         */
        return function (input) {
            if ('string' !== typeof input) {
                return null;
            }

            try {
                return input.pluralize();
            } catch (e) {
                throw new Error('The "pluralize" filter required "inflectionjs" library.');
            }
        }
    })
    .filter('singularize', function () {
        /**
         * Renders a plural English language noun into its singular form
         * normal results can be overridden by passing in an alterative
         *
         * @param {String} input
         *
         * @return {String}
         */
        return function (input) {
            if ('string' !== typeof input) {
                return null;
            }

            try {
                return input.singularize();
            } catch (e) {
                throw new Error('The "singularize" filter required "inflectionjs" library.');
            }
        }
    })
    .filter('camelize', function () {
        /**
         * Renders a lower case underscored word into camel case
         * the first letter of the result will be upper case unless you pass true
         * also translates "/" into "::" (underscore does the opposite)
         *
         * @param {String} input
         *
         * @return {String}
         */
        return function (input) {
            if ('string' !== typeof input) {
                return null;
            }

            try {
                return input.camelize();
            } catch (e) {
                throw new Error('The "camelize" filter required "inflectionjs" library.');
            }
        }
    })
    .filter('underscore', function () {
        /**
         * Renders a camel cased word into words seperated by underscores
         * also translates "::" back into "/" (camelize does the opposite)
         *
         * @param {String} input
         *
         * @return {String}
         */
        return function (input) {
            if ('string' !== typeof input) {
                return null;
            }

            try {
                return input.underscore();
            } catch (e) {
                throw new Error('The "underscore" filter required "inflectionjs" library.');
            }
        }
    })
    .filter('humanize', function () {
        /**
         * Renders a lower case and underscored word into human readable form
         * defaults to making the first letter capitalized unless you pass true
         *
         * @param {String} input
         *
         * @return {String}
         */
        return function (input) {
            if ('string' !== typeof input) {
                return null;
            }

            try {
                return input.humanize();
            } catch (e) {
                throw new Error('The "humanize" filter required "inflectionjs" library.');
            }
        }
    })
    .filter('capitalize', function () {
        /**
         * Renders all characters to lower case and then makes the first upper
         *
         * @param {String} input
         *
         * @return {String}
         */
        return function (input) {
            if ('string' !== typeof input) {
                return null;
            }

            try {
                return input.capitalize();
            } catch (e) {
                throw new Error('The "capitalize" filter required "inflectionjs" library.');
            }
        }
    })
    .filter('dasherize', function () {
        /**
         * Renders all underbars and spaces as dashes
         *
         * @param {String} input
         *
         * @return {String}
         */
        return function (input) {
            if ('string' !== typeof input) {
                return null;
            }

            try {
                return input.dasherize();
            } catch (e) {
                throw new Error('The "dasherize" filter required "inflectionjs" library.');
            }
        }
    })
    .filter('titleize', function () {
        /**
         * Renders words into title casing (as for book titles)
         *
         * @param {String} input
         *
         * @return {String}
         */
        return function (input) {
            if ('string' !== typeof input) {
                return null;
            }

            try {
                return input.titleize();
            } catch (e) {
                throw new Error('The "titleize" filter required "inflectionjs" library.');
            }
        }
    })
    .filter('demodulize', function () {
        /**
         * Renders class names that are prepended by modules into just the class
         *
         * @param {String} input
         *
         * @return {String}
         */
        return function (input) {
            if ('string' !== typeof input) {
                return null;
            }

            try {
                return input.demodulize();
            } catch (e) {
                throw new Error('The "demodulize" filter required "inflectionjs" library.');
            }
        }
    })
    .filter('tableize', function () {
        /**
         * Renders camel cased singular words into their underscored plural form
         *
         * @param {String} input
         *
         * @return {String}
         */
        return function (input) {
            if ('string' !== typeof input) {
                return null;
            }

            try {
                return input.tableize();
            } catch (e) {
                throw new Error('The "tableize" filter required "inflectionjs" library.');
            }
        }
    })
    .filter('classify', function () {
        /**
         * Renders an underscored plural word into its camel cased singular form
         *
         * @param {String} input
         *
         * @return {String}
         */
        return function (input) {
            if ('string' !== typeof input) {
                return null;
            }

            try {
                return input.classify();
            } catch (e) {
                throw new Error('The "classify" filter required "inflectionjs" library.');
            }
        }
    })
    .filter('foreign_key', function () {
        /**
         * Renders a class name (camel cased singular noun) into a foreign key
         * defaults to seperating the class from the id with an underbar unless
         * you pass true
         *
         * @param {String} input
         *
         * @return {String}
         */
        return function (input) {
            if ('string' !== typeof input) {
                return null;
            }

            try {
                return input.foreign_key();
            } catch (e) {
                throw new Error('The "foreign_key" filter required "inflectionjs" library.');
            }
        }
    })
    .filter('ordinalize', function () {
        /**
         * Renders all numbers found in the string into their sequence like "22nd"
         *
         * @param {String} input
         * @return {String}
         */
        return function (input) {
            if ('string' !== typeof input) {
                return null;
            }

            try {
                return input.ordinalize();
            } catch (e) {
                throw new Error('The "ordinalize" filter required "inflectionjs" library.');
            }
        }
    })
;
