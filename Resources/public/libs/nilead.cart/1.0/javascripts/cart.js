angular.module('nilead.cart.cart', ['nilead.common', 'nilead.cart.cart_item'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })

    .service('CartRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'cart',
            resource: 'cart',
            params: {
                _format: 'partial'
            }
        });
    }])

    .service('CartResponse', ['ResponseFactory', 'CartRouter', function (ResponseFactory, CartRouter) {
        return new ResponseFactory(CartRouter);
    }])

    .service('CartCommon', function () {
        return function (Resource, CartRouter, CartResponse) {
            return {
                clearCart: function (getParams, postParams, success, error) {
                    var event = this.getEvent('clear');
                    return Resource.post(CartRouter.generate('clear', getParams), postParams,
                        function (response) {
                            return CartResponse.success(response, success, event);
                        },
                        function (response) {
                            return CartResponse.error(response, error, event);
                        });
                },
                saveCart: function (getParams, postParams, success, error) {
                    var event = this.getEvent('save');
                    return Resource.post(CartRouter.generate('save', getParams), postParams,
                        function (response) {
                            return CartResponse.success(response, success, event);
                        },
                        function (response) {
                            return CartResponse.error(response, error, event);
                        });
                },
                addDiscount: function(getParams, postParams, success, error) {
                    var event = this.getEvent('add_discount');
                    return Resource.post(CartRouter.generate('add_discount', getParams), postParams,
                        function (response) {
                            return CartResponse.success(response, success, event);
                        },
                        function (response) {
                            return CartResponse.error(response, error, event);
                        });
                },
                removeDiscount: function(getParams, postParams, success, error) {
                    var event = this.getEvent('remove_discount');
                    return Resource.post(CartRouter.generate('remove_discount', getParams), postParams,
                        function (response) {
                            return CartResponse.success(response, success, event);
                        },
                        function (response) {
                            return CartResponse.error(response, error, event);
                        });
                }
            }
        }
    })
    .service('Cart', ['ResourceFactory', 'CartRouter', 'CartResponse', 'CartCommon', function (ResourceFactory, CartRouter, CartResponse, CartCommon) {
        return new ResourceFactory(CartRouter, CartResponse, CartCommon);
    }])

    .directive('nlClearCart', ['$compile', 'Cart', function ($compile, Cart) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('click', function (e) {
                    var template = {};
                    if (attrs.nlTemplate != null) {
                        template = { 'template': attrs.nlTemplate }
                    }
                    var target = angular.element(attrs.nlTarget);
                    Cart.clearCart(template, function (response) {
                        target.html(response.data.template);
                        $compile(target.contents())(scope);

                        return false;
                    })
                });
            }
        }
    }])
    .directive('nlCartSave', ['$compile', 'Cart', '$parse', function ($compile, Cart, $parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('click', function (e) {
                    var target = angular.element(attrs.nlTarget);
                    var template = {};
                    if (attrs.nlTemplate != null) {
                        template = { 'template': attrs.nlTemplate }
                    }
                    var form = $parse(attrs.nlCartSave)(scope);
                    Cart.saveCart(form, template, function (response) {
                        target.html(response.data.template);
                        $compile(target.contents())(scope)
                        return false;
                    })
                });
            }
        }
    }])
;
