angular.module('nilead.frontend.address')
    .service('AddressRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'address',
            resource: 'address',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('AddressResponse', ['ResponseFactory', 'AddressRouter', function (ResponseFactory, AddressRouter) {
        return new ResponseFactory(AddressRouter);
    }])
    .service('AddressFrontend', ['ResourceFactory', 'AddressRouter', 'AddressResponse', function (ResourceFactory, AddressRouter, AddressResponse) {
        return new ResourceFactory(AddressRouter, AddressResponse);
    }])
;
