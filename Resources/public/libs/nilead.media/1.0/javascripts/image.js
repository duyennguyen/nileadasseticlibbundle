angular.module('nilead.media.image', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('ImageRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'media',
            resource: 'image',
            params: {
                _format: 'partial'
            }
        });
    }])
    .factory('ImageAwareResponseFactory', [function () {

        return function (ResponseObject, processImages) {

            processImages = 'function' == typeof processImages ? processImages : function (response, resource) {
                var path = ['data', 'data', resource, 'images'], images = {};

                if (_.hasPath(response, path)) {
                    angular.forEach(_.getPath(response, path), function (val, key) {
                        images[val.id] = val;
                    });
                }

                return images;

            };

            var o = Object.create(ResponseObject, {
                $images: {
                    value: {},
                    writable: true,
                    enumerable: true,
                    configurable: true
                }
            });

            o.addImages = function (images) {
                angular.forEach(images, function (val, key) {
                    o.$images[val.id] = val;
                });
            };

            o.removeImage = function (id) {
                if (o.$images.hasOwnProperty(id)) {
                    delete o.$images[id];
                }
            };

            o.getImages = function () {
                return o.$images;
            };

            // override success
            o.success = function (response, callback, event) {
                o.$images = processImages(response, event.resource);

                return this.__proto__.success(response, callback, event);
            };

            return o;
        }
    }])
    .service('ImageResponse', ['ResponseFactory', 'ImageRouter', function (ResponseFactory, ImageRouter) {
        return new ResponseFactory(ImageRouter);
    }])

    .service('Image', ['ResourceFactory', 'ImageRouter', 'ImageResponse', function (ResourceFactory, ImageRouter, ImageResponse) {
        return new ResourceFactory(ImageRouter, ImageResponse);
    }])
    .service('ImageBulk', ['Image', 'BulkFactory', function (Image, BulkFactory) {
        return new BulkFactory(Image, {
            actions: {
                enable: null,
                disable: null,
                delete: null
            }
        });
    }])
    .service('ImageSearch', ['ImageRouter', 'SearchFactory', function (ImageRouter, SearchFactory) {

        return function (id, preset, action) {
            if (!angular.isDefined(action)) {
                action = 'index';
            }

            return SearchFactory(id, {
                router: ImageRouter,
                action: action
            }, 'image', preset);
        }
    }])
    .directive('nlMediaManager', ['ImageSearch', 'NILEAD_EVENTS', 'EventListener', '$compile', 'Image', 'ResponseProcessor', function (ImageSearch, NILEAD_EVENTS, EventListener, $compile, Image, ResponseProcessor) {
        return {
            scope: {
                nlSelectMode: '@',
                nlModel: '=',
                nlImages: '='
            },
            link: function (scope, element, attrs) {
                var images = scope.nlImages || {},
                    template = (attrs.nlTemplate || 'media_manager.html').replace(/\./g, '\\.'),
                    selectMode = scope.nlSelectMode || 'multiple',
                    // do we want to use the image path instead of the image object
                    imagePath = attrs.nlImagePath || false,
                    paramImage = attrs.paramImage || 'image_manager';

                // search
                var searcher = ImageSearch('mediaImageManagerSearcher', 'default', 'index');

                var params = {
                    _format: 'partial',
                    template: paramImage,
                    include_metadata: 1,
                    include_full_agg: 0
                };

                //

                scope.mode = "browse";
                scope.modelImages = {};
                scope.currentImage = false;
                scope.selectedImages = {};

                scope.showModal = function () {
                    element.find('.modal').modal('show');
                };

                scope.saveForm = function (id, form) {
                    Image.update(id, form, false, false)
                        .then(function (response) {
                            return response.data.data.image;
                        })
                        .then(function(image) {
                            images[id] = image;
                            scope.currentImage = false;
                        })
                    ;
                };

                // trigger edit mode from normal view
                scope.editImage = function(id) {
                    scope.showModal();
                    scope.switchMode('edit');
                    scope.select(id);
                };

                scope.removeImage = function(id) {
                    if ('multiple' == scope.nlSelectMode) {
                        delete scope.modelImages[id];

                        if ('undefined' !==  typeof scope.nlModel) {
                            scope.nlModel = _.allKeys(scope.modelImages);
                        }
                    } else {
                        scope.modelImages = {};

                        if ('undefined' !==  typeof scope.nlModel) {
                            scope.nlModel = '';
                        }
                    }

                    scope.search(searcher.$lastParams.page, true);
                };

                scope.deleteImage = function(id) {
                    Image.delete({id: id}, {}, false, false)
                        .then(function (response) {
                            return response.data.data.image;
                        })
                        .then(function(image) {
                            scope.currentImage = false;
                            scope.search(searcher.$lastParams.page, true);
                        })
                    ;
                };

                scope.switchMode = function (newMode) {
                    scope.mode = newMode;
                };

                scope.addImages = function (newImages) {
                    angular.forEach(newImages, function (val, key) {
                        images[val.id] = val;
                    });
                };

                scope.cancelEdit = function () {
                    scope.currentImage = false;
                };

                // this method is used to update the model with the current list of selected images
                scope.update = function () {
                    if ('multiple' == scope.nlSelectMode) {
                        scope.nlModel = _.allKeys(scope.selectedImages);
                    } else {
                        if(imagePath) {
                            for (var key in scope.selectedImages) {
                                scope.nlModel = scope.selectedImages[key].path;
                            }
                        }else {
                            scope.nlModel = _.allKeys(scope.selectedImages)[0];
                        }
                    }
                    angular.extend(scope.modelImages, scope.selectedImages);

                    // reset selected
                    //scope.selectedImages = {};
                };

                // this method unSelect an image in browse mode
                scope.unselect = function (id) {
                    delete scope.selectedImages[id];
                };

                // this method select an image in browse mode
                scope.select = function (id) {
                    if ('browse' == scope.mode) {
                        if ('single' == selectMode) {
                            if (images.hasOwnProperty(id)) {
                                scope.selectedImages = {};
                                scope.selectedImages[id] = images[id];
                            }
                        } else {
                            if (!scope.selectedImages.hasOwnProperty(id)) {
                                if (images.hasOwnProperty(id)) {
                                    scope.selectedImages[id] = images[id];
                                }
                            } else {
                                delete scope.selectedImages[id];
                            }
                        }
                    } else {
                        if (images.hasOwnProperty(id)) {
                            scope.currentImage = images[id];
                            // Loop through nilead_image model & get new value from selected image
                            for (var prop in scope.form.nilead_image) {
                                if (images[id][prop]) {
                                    scope.form.nilead_image[prop] = images[id][prop];
                                }
                            }
                        }
                    }
                };

                // check if the given id is selected
                scope.selected = function (id) {
                    return scope.mode == 'browse' && scope.selectedImages.hasOwnProperty(id);
                };

                // bulk actions
                scope.bulks = [
                    {value: '', label: 'Please select'},
                    {value: 'delete', label: 'Delete'}
                ];

                scope.BulkAction = function() {
                    if(!_.isEmpty(scope.selectedImages)) {
                        switch (scope.bulk.value) {
                            case 'delete':
                                Image.delete({ids: _.allKeys(scope.selectedImages)}, {}, function (response) {
                                    //scope.images = response.data.data.images;
                                    angular.forEach(scope.selectedImages, function (val, key) {
                                        scope.removeImage(key);
                                    });
                                    scope.selectedImages = {};
                                    //ImageSearch('mediaImageManagerSearcher', 'default', 'index').search(1, true);
                                    return false;
                                }, false);

                                break;
                        }
                    }
                };

                // init selected images
                if ('undefined' !== typeof scope.nlModel) {
                    if ('multiple' == scope.nlSelectMode) {
                        angular.forEach(scope.nlModel, function (val, key) {
                            scope.modelImages[val] = images[val];
                            scope.select(val);
                        });
                    } else {
                        scope.modelImages[scope.nlModel] = images[scope.nlModel];
                        scope.select(scope.nlModel);
                    }
                }

                scope.uploadComplete = function($flow) {
                    var file = _.last($flow.files);

                    if (!file.error) {
                        ResponseProcessor.processSuccess({data: JSON.parse(_.last(file.chunks).xhr.response)});
                        scope.$broadcast('uploadComplete');
                    }
                };

                scope.uploadError = function($flow, $message) {
                    ResponseProcessor.processError({data: JSON.parse($message)});
                };

                // search
                scope.paginationCallback = function (page, $event) {
                    this.search(page);
                    $event.preventDefault();
                };

                scope.search = function (page, reload) {

                    params.page = page;

                    if ('undefined' !== typeof scope.value) {
                        params.q = {
                            queries: {
                                name_match: {
                                    value: scope.value
                                }
                            }
                        };
                    }

                    searcher.search(params, reload,
                        function (response) {
                            var result = element.find('.nl-search-result');
                            result.html(response.data.template);
                            $compile(result.contents())(scope);

                            if (_.hasPath(response, ['data', 'data', 'images'])) {
                                scope.images = response.data.data.images;
                                scope.addImages(response.data.data.images);
                            }

                            return false;
                        }
                    );
                };

                // listen to upload event
                scope.$on('uploadComplete', function() {
                    searcher.resetCache();
                    scope.search(1, true);
                });

                EventListener.once(NILEAD_EVENTS.SEARCH_UPDATE_DATA + searcher.getId(), function () {
                    scope.search();
                });

                element.html(angular.element('#' + template).html());
                $compile(element.contents())(scope);

                scope.search(1);
            }
        }
    }])
;
