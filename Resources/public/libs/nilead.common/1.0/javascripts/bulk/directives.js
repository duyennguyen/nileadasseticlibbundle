angular.module('nilead.common')
    .directive('nlBulk', function () {
        return {
            restrict: 'A',
            templateUrl: 'bulk.html',
            scope: {
                nlBulk: '='
            }
        }
    })
    .directive('nlBulkItem', function () {
        return {
            restrict: 'A',
            scope: {
                nlBulk: '=nlBulker'
            },
            link: function (scope, element, attrs) {
                scope.nlBulk.addItem(attrs.nlBulkItem);

                element.bind('click', function (event) {
                    scope.$apply(function() {
                        scope.nlBulk.toggleItem(attrs.nlBulkItem);
                    });
                });
            }
        }
    })
;
