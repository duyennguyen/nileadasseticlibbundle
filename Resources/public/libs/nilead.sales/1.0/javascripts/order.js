angular.module('nilead.sales.order', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('OrderRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'sales',
            resource: 'order',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('OrderResponse', ['ResponseFactory', 'OrderRouter', function (ResponseFactory, OrderRouter) {
        return new ResponseFactory(OrderRouter);
    }])
    .service('Order', ['ResourceFactory', 'OrderRouter', 'OrderResponse', 'Resource', function (ResourceFactory, OrderRouter, OrderResponse, Resource) {
        return new ResourceFactory(OrderRouter, OrderResponse, function(Resource, OrderRouter, OrderResponse){
            return {
                updateSummary: function(id, summary, success, error) {
                    var event = this.getEvent('update_summary');
                    return Resource.post(OrderRouter.generate('update_summary', { id: id, _format: 'json' }), summary,
                        function (response) {
                            return OrderResponse.success(response, success, event);
                        },
                        function (response) {
                            return OrderResponse.error(response, error, event);
                        });
                }};
        });
    }])
    .service('OrderBulk', ['Order', 'BulkFactory', function (Order, BulkFactory) {
        return new BulkFactory(Order, {
            actions: {
                active: null
            }
        });
    }])
;
