angular.module('nilead.navigation.navigation', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('NavigationRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'navigation',
            resource: 'navigation',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('NavigationResponse', ['ResponseFactory', 'NavigationRouter', function (ResponseFactory, NavigationRouter) {
        return new ResponseFactory(NavigationRouter);
    }])
    .service('Navigation', ['ResourceFactory', 'NavigationRouter', 'NavigationResponse', function (ResourceFactory, NavigationRouter, NavigationResponse) {
        return new ResourceFactory(NavigationRouter, NavigationResponse);
    }])
    .service('NavigationBulk', ['Navigation', 'BulkFactory', function (Navigation, BulkFactory) {
        return new BulkFactory(Navigation, {
            actions: {
                delete: null
            }
        });
    }])
;
