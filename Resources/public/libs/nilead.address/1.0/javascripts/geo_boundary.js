angular.module('nilead.address.geo_boundary', [])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('GeoBoundaryRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'address',
            resource: 'geo_boundary',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('GeoBoundaryResponse', ['ResponseFactory', 'GeoBoundaryRouter', function (ResponseFactory, AddressRouter) {
        return new ResponseFactory(AddressRouter);
    }])
    .service('GeoBoundary', ['ResourceFactory', 'GeoBoundaryRouter', 'GeoBoundaryResponse', function (ResourceFactory, GeoBoundaryRouter, GeoBoundaryResponse) {
        return new ResourceFactory(GeoBoundaryRouter, GeoBoundaryResponse, function (Resource, GeoBoundaryRouter, GeoBoundaryResponse) {
            return {
                getGeoBoundaryCountries: function (success, error) {
                    var event = this.getEvent('all_countries');
                    return Resource.get(GeoBoundaryRouter.generate('all_countries'),
                        function (response) {
                            return GeoBoundaryResponse.success(response, success, event);
                        },
                        function (response) {
                            return GeoBoundaryResponse.error(response, error, event);
                        },
                        function (RestangularConfigurer) {
                            RestangularConfigurer.setDefaultHttpFields({cache: true});
                        })
                },
                getGeoBoundaryMembers: function (id, success, error) {
                    var event = this.getEvent('get_members');
                    return Resource.get(GeoBoundaryRouter.generate('get_members', {id: id}),
                        function (response) {
                            return GeoBoundaryResponse.success(response, success, event);
                        },
                        function (response) {
                            return GeoBoundaryResponse.error(response, error, event);
                        },
                        function (RestangularConfigurer) {
                            RestangularConfigurer.setDefaultHttpFields({cache: true});
                        });
                }
            }
        });
    }])
