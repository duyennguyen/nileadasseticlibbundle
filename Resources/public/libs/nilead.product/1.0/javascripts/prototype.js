angular.module('nilead.product.prototype', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('PrototypeRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'product',
            resource: 'prototype',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('PrototypeResponse', ['ResponseFactory', 'PrototypeRouter', function (ResponseFactory, PrototypeRouter) {
        return new ResponseFactory(PrototypeRouter);
    }])
    .service('Prototype', ['ResourceFactory', 'PrototypeRouter', 'PrototypeResponse', function (ResourceFactory, PrototypeRouter, PrototypeResponse) {
        return new ResourceFactory(PrototypeRouter, PrototypeResponse);
    }])
    .service('PrototypeBulk', ['Prototype', 'BulkFactory', function (Prototype, BulkFactory) {
        return new BulkFactory(Prototype, {
            actions: {
                delete: null
            }
        });
    }])
;
