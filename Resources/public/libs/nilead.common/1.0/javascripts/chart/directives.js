angular.module('nilead.common')
    /**
     * @ngdoc: directive
     * @name: nilead.common:nlChart
     * @scope
     * @param {object} nlChart takes the same data with the corresponding ``data` on Chartis
     * @param {object} nlOptions takes the same data with the corresponding ``options` on Chartis
     * @param {string} nlType the chart type which defaults to 'Line' (available types are 'Line', 'Bar', 'Pie').
     *
     * @restrict A
     *
     * @description
     * Draw a chart from given input data using Chartis(http://gionkunz.github.io/chartist-js/)
     * Apps that use this directive should include nilead.common
     *
     * @example
     *
     * <pre>
     *      <div class="ct-chart" data-nl-options="{height: 600}" data-nl-chart="dashboard.orderTotalByMonth" data-nl-type="Pie"></div>
     * </pre>
     *
     */
     .directive('nlChart', function () {
        var types = ['Line', 'Bar', 'Pie'];

        var helper = {
            parseOrderTotal: function (data) {
                var result = {'labels': data.labels, series: []};

                angular.forEach(data.series, function (value, key) {
                    result.series.push(value.values);
                });

                return result;
            }
        };

        return {
            scope: {
                nlChart: '=',
                nlOptions: '=',
                nlType: '@'
            },
            restrict: 'A',
            link: function (scope, element, attrs) {
                var options = {};
                if ('string' === typeof scope.nlOptions) {

                    if (scope.$parent.hasOwnProperty(scope.nlOptions)) {
                        options = scope.$parent[scope.nlOptions];
                    }
                } else {
                    options = scope.nlOptions;
                }

                if (-1 == _.indexOf(types, scope.nlType)) {
                    scope.nlType = types[0];
                }

                var data = helper.parseOrderTotal(scope.nlChart);

                new Chartist[scope.nlType](element.get(0), {
                        labels: data.labels,
                        series: data.series
                    }, options)
                ;
            }
        };
    });
