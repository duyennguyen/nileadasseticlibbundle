angular.module('nilead.security.client', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('ClientRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'security',
            resource: 'client',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('ClientResponse', ['ResponseFactory', 'ClientRouter', function (ResponseFactory, ClientRouter) {
        return new ResponseFactory(ClientRouter);
    }])
    .service('Client', ['ResourceFactory', 'ClientRouter', 'ClientResponse', function (ResourceFactory, ClientRouter, ClientResponse) {
        return new ResourceFactory(ClientRouter, ClientResponse);
    }])
    .service('ClientBulk', ['Client', 'BulkFactory', function (Client, BulkFactory) {
        return new BulkFactory(Client, {
            actions: {
                delete: null
            }
        });
    }])
;
