angular.module('nilead.inventory.inventory')
    .directive('nlAddAdjustment',['Inventory', '$parse', '$compile', function(Inventory, $parse, $compile){
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('click', function(e) {
                    var target = angular.element(attrs.nlTarget);
                    Inventory.addDiscountCode(attrs.nlClientKey, $parse(attrs.nlModel)(scope), function (response) {
                        if (undefined !== response.data.template) {
                            target.html(response.data.template);
                            $compile(target.contents())(scope);
                        }
                    })
                })
            }
        }
    }]);
