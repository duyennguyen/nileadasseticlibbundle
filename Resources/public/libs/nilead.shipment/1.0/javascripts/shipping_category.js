angular.module('nilead.shipment.shipping_category', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('ShippingCategoryRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'shipment',
            resource: 'shipping_category',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('ShippingCategoryResponse', ['ResponseFactory', 'ShippingCategoryRouter', function (ResponseFactory, ShippingCategoryRouter) {
        return new ResponseFactory(ShippingCategoryRouter);
    }])
    .service('ShippingCategory', ['ResourceFactory', 'ShippingCategoryRouter', 'ShippingCategoryResponse', function (ResourceFactory, ShippingCategoryRouter, ShippingCategoryResponse) {
        return new ResourceFactory(ShippingCategoryRouter, ShippingCategoryResponse);
    }])
;
