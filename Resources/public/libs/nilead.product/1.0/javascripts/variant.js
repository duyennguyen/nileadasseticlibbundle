angular.module('nilead.product.variant', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('VariantRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'product',
            resource: 'variant',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('VariantResponse', ['ResponseFactory', 'VariantRouter', function (ResponseFactory, VariantRouter) {
        return new ResponseFactory(VariantRouter);
    }])
    .service('Variant', ['ResourceFactory', 'VariantRouter', 'VariantResponse', function (ResourceFactory, VariantRouter, VariantResponse) {
        return new ResourceFactory(VariantRouter, VariantResponse);
    }])
    .service('VariantBulk', ['Variant', 'BulkFactory', function (Variant, BulkFactory) {
        return new BulkFactory(Variant, {
            actions: {
                enable: null,
                disable: null,
                delete: null
            }
        });
    }])
    .service('VariantSearch', ['VariantRouter', 'SearchFactory', function (VariantRouter, SearchFactory) {
        return function (id, preset, action) {
            if (!angular.isDefined(action)) {
                action = 'index';
            }

            return SearchFactory(id, {
                action: action,
                router: VariantRouter,
                entities: [VariantRouter.getResource()]
            }, 'variant', preset);
        }
    }])
    .service('VariantGenerator', [function () {
        return {
            /**
             * Generate variants from option and option values choice list
             *
             * @param {Array} choices option and option values choice
             *
             * @return {Array}
             */
            getVariantCombinations: function (choices) {
                // checks for all options are selected.
                var optionId, optionValues, combinations = [];
                for (optionId in choices) {
                    optionValues = choices[optionId];

                    // return empty generated variants if one of option value is missing
                    if (undefined === optionValues || 0 === optionValues.length) {
                        return [];
                    }

                    combinations = this.mergeOptionValues(combinations, optionId, optionValues);
                }

                return combinations;
            },

            /**
             * Merge option values for each variant
             *
             * @param  {array}   variants     current variant
             * @param  {int} optionId     option id
             * @param  {array}   optionValues option values
             *
             * @return array
             */
            mergeOptionValues: function (variants, optionId, optionValues) {
                var i, j, variant, newVariants = [];

                if (0 === variants.length) {
                    for (i in optionValues) {
                        newVariants.push([
                            [optionId, optionValues[i]]
                        ]);
                    }
                } else {
                    for (i in variants) {
                        for (j in optionValues) {
                            // clone variant from variants[i]
                            variant = variants[i].slice(0);
                            variant.push([optionId, optionValues[j]]);

                            newVariants.push(variant);
                        }
                    }
                }

                return newVariants;
            },

            /**
             * Generate variant name using list of product options & current variant option values
             *
             * @param options
             * @param selectedValues
             *
             * @returns {string}
             */
            generateName: function (options, selectedValues) {
                var name = [];

                for (var i = 0, selectedValue; selectedValue = selectedValues[i]; i++) {
                    var option = options.filter(function (option) {
                        return parseInt(selectedValue[0]) === option.id;
                    })[0];

                    var optionValue = option.values.filter(function (optionValue) {
                        return parseInt(selectedValue[1]) === optionValue.id;
                    })[0];

                    name.push(optionValue.value);
                }

                return name.join(' • ');
            },

            generateOptionValues: function (variantCombination) {
                var optionValues = {};

                for (var i = 0, combination; combination = variantCombination[i]; i++) {
                    optionValues[combination[0]] = [combination[1]];
                }

                return optionValues;
            },
            //
            //generateKey: function (optionValues) {
            //    var key = '';
            //    angular.forEach(optionValues, function (values, optionId) {
            //        key += values.sort().join('_') + '_';
            //    });
            //
            //    return key;
            //},

            /**
             *
             * @param product
             * @param optionValues
             *
             * @returns {Array}
             */
            generate: function (product, optionValues) {
                var _self = this;
                var variants = [];
                var variantCombinations = this.getVariantCombinations(optionValues);

                angular.forEach(variantCombinations, function (variantCombination) {
                    var name = _self.generateName(product.options, variantCombination);
                    var optionValues = _self.generateOptionValues(variantCombination);

                    variants.push({
                        name: name,
                        options: optionValues,
                        // Do not need to handle price component default value here
                        priceComponent: {
                            calculator: 'default'
                        }
                    });
                });

                return variants;
            }
        }
    }])
;
