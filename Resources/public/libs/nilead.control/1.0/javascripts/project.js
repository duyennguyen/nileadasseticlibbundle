angular.module('nilead.control.project', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('ProjectRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'control',
            resource: 'project',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('ProjectResponse', ['ResponseFactory', 'ProjectRouter', function (ResponseFactory, ProjectRouter) {
        return new ResponseFactory(ProjectRouter);
    }])
    .service('Project', ['ResourceFactory', 'ProjectRouter', 'ProjectResponse', function (ResourceFactory, ProjectRouter, ProjectResponse) {
        return new ResourceFactory(ProjectRouter, ProjectResponse);
    }])
;
