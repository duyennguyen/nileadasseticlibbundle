angular.module('nilead.payment.payment_method', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('PaymentMethodRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'payment',
            resource: 'payment_method',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('PaymentMethodResponse', ['ResponseFactory', 'PaymentMethodRouter', function (ResponseFactory, PaymentMethodRouter) {
        return new ResponseFactory(PaymentMethodRouter);
    }])
    .service('PaymentMethod', ['ResourceFactory', 'PaymentMethodRouter', 'PaymentMethodResponse', function (ResourceFactory, PaymentMethodRouter, PaymentMethodResponse) {
        return new ResourceFactory(PaymentMethodRouter, PaymentMethodResponse);
    }])
;
