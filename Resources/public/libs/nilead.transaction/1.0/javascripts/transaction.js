angular.module('nilead.transaction.transaction', ['nilead.common'])
    .config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}')
    })
    .service('TransactionRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'payment',
            resource: 'transaction',
            params: {
                _format: 'partial'
            }
        });
    }])
    .service('TransactionResponse', ['ResponseFactory', 'TransactionRouter', function (ResponseFactory, TransactionRouter) {
        return new ResponseFactory(TransactionRouter);
    }])
    .service('Transaction', ['ResourceFactory', 'TransactionRouter', 'TransactionResponse', 'Resource', function (ResourceFactory, TransactionRouter, TransactionResponse, Resource) {
        return new ResourceFactory(TransactionRouter, TransactionResponse, function(Resource, TransactionRouter, TransactionResponse){
            return {
                capture: function (id, success, error)  {
                    var event = this.getEvent('capture');
                    return Resource.post(TransactionRouter.generate('capture', {id: id, _format:'json'}),
                        function (response) {
                            return TransactionResponse.success(response, success, event);
                        },
                        function (response) {
                            return TransactionResponse.error(response, error, event);
                        }
                    );
                },
                void: function (id, success, error) {
                    var event = this.getEvent('void');
                    return Resource.post(TransactionRouter.generate('void', {id: id, _format:'json'}),
                        function (response) {
                            return TransactionResponse.success(response, success, event);
                        },
                        function (response) {
                            return TransactionResponse.error(response, error, event);
                        }
                    );
                }
            }
        });
    }])
    .service('TransactionBulk', ['Transaction', 'BulkFactory', function (Transaction, BulkFactory) {
        return new BulkFactory(Transaction, {
            actions: {
                active: null
            }
        });
    }])
;
