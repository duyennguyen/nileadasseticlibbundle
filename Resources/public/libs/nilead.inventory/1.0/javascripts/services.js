angular.module('nilead.inventory.inventory')
    .service('InventoryRouter', ['RouterFactory', function (RouterFactory) {
        return new RouterFactory({
            app: 'nilead',
            bundle: 'inventory',
            resource: 'inventory',
            params: {
                _format: 'partial'
            }
        });
    }])

    .service('InventoryResponse', ['ResponseFactory', 'InventoryRouter', function (ResponseFactory, InventoryRouter) {
        return new ResponseFactory(InventoryRouter);
    }])
    .service('Inventory', ['ResourceFactory', 'InventoryRouter', 'InventoryResponse', function (ResourceFactory, InventoryRouter, InventoryResponse) {
        return new ResourceFactory(InventoryRouter, InventoryResponse);
    }]);

